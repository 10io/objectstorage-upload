## Direct Upload

In this mode, we can summarize workhorse - rails interactions in two requests:
1. Authorize (route ending with `/authorize`)
   * Workhorse asking: hey, I have to upload this file where do you want me to put it?
1. Finalize (whatever route)
   * Workhorse saying: hey, I uploaded this file, now do your thing

Workhorse will upload the file to the location provided by rails between (1.) and (2.)

### Setup

For body uploads, we're going to use generic package uploads.

For multipart uploads, we're going to use NuGet package uploads. The form key used for the file is `package`.

Non consolidated form is used for the configuration.

### Local

Object storage is actually the file system itself.

```yaml
  packages:
    enabled: true
    dpkg_deb_path: /usr/local/bin/dpkg-deb
    object_store:
      enabled: false
      remote_directory: packages
      direct_upload: true
```

#### Body Upload

1. (WH) Original request:
   ```
   PUT /api/v4/projects/509/packages/generic/my_package/0.0.1/file.txt HTTP/1.1
   Host: gdk.test:8000
   Accept: */*
   Content-Length: 8
   Expect: 100-continue
   Private-Token: <PAT>
   User-Agent: curl/7.64.1
   
   bananas
   ```
1. (WH) Authorize request:
   ```
   PUT /api/v4/projects/509/packages/generic/my_package/0.0.1/file.txt/authorize HTTP/0.0
   Host: gdk.test:8000
   Accept: */*
   Expect: 100-continue
   Gitlab-Workhorse: 11-10-0cfa69752d8-74ffd66ae-ee-135594-g237b6bf5efe
   Gitlab-Workhorse-Api-Request: eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpc3MiOiJnaXRsYWItd29ya2hvcnNlIn0.y__tLHV0C92bicEJQZuGo0HGqbbJ0EbPY9Nv5bI0jKs
   Private-Token: <PAT>
   User-Agent: curl/7.64.1
   
   
   ```
1. (Rails) _No object storage interactions_
1. (WH) Authorize response:
   ```
   Authorize Response
   HTTP/1.0 200 OK
   Connection: close
   Cache-Control: max-age=0, private, must-revalidate
   Content-Type: application/vnd.gitlab-workhorse+json
   Etag: W/"bceb23d4a296a364ade854e9db731521"
   Vary: Origin
   X-Content-Type-Options: nosniff
   X-Frame-Options: SAMEORIGIN
   X-Request-Id: 01FPASCYV34WN4TVQWP48JHK8T
   X-Runtime: 0.582073
   
   {"TempPath":"/Users/david/projects/gitlab-development-kit/gitlab/shared/packages/tmp/uploads"}
   ```
1. (WH) Finalize request:
   ```
   PUT /api/v4/projects/509/packages/generic/my_package/0.0.1/file.txt HTTP/1.1
   Host: gdk.test:8000
   Accept: */*
   Content-Length: 8
   Content-Type: application/x-www-form-urlencoded
   Expect: 100-continue
   Gitlab-Workhorse-Multipart-Fields: eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJyZXdyaXR0ZW5fZmllbGRzIjp7ImZpbGUiOiIvVXNlcnMvZGF2aWQvcHJvamVjdHMvZ2l0bGFiLWRldmVsb3BtZW50LWtpdC9naXRsYWIvc2hhcmVkL3BhY2thZ2VzL3RtcC91cGxvYWRzLzg2NjU1NDIxNCJ9LCJpc3MiOiJnaXRsYWItd29ya2hvcnNlIn0.zF6pp5-Q94tQHEE9DW8f2G7jCjxsh8KfWzNkF0bk7vY
   Private-Token: <PAT>
   User-Agent: curl/7.64.1
   
   file.gitlab-workhorse-upload=eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1cGxvYWQiOnsibWQ1IjoiM2UyNDJmYjcxNGQ4N2IzYzExMGM4NjQ3NmY1ZmY5NzIiLCJuYW1lIjoiIiwicGF0aCI6Ii9Vc2Vycy9kYXZpZC9wcm9qZWN0cy9naXRsYWItZGV2ZWxvcG1lbnQta2l0L2dpdGxhYi9zaGFyZWQvcGFja2FnZXMvdG1wL3VwbG9hZHMvODY2NTU0MjE0IiwicmVtb3RlX2lkIjoiIiwicmVtb3RlX3VybCI6IiIsInNoYTEiOiI5NzAzMTg5NjhmZWI2NDBkYTcyM2I4ODI2ODYxZTQxZjA3MThhNDg3Iiwic2hhMjU2IjoiZTQ5Mjk1NzAyZjdkYTg2NzA3NzhlOWI5NWEyODFiNzJiNDFiMzFjYjE2YWZhMzc2MDM0YjQ1ZjU5YTE4ZWEzZiIsInNoYTUxMiI6IjgyOTA1NmQ0ZmQzMDJjZjJmZGI0M2Q5ZGNiYjBhNDBjZjBjY2VkZTcyY2QzZmE0N2I0MjBiMTVmNDgxOGM1MzQwMDQ3NzliMjEwZDdiNjk5ODY2MTg5Yjk3ZmNkZWI1YzdjOWM5ZmEzZTJkZWNjYzMyNzRiMzRlODVlYWQzMTY0Iiwic2l6ZSI6IjgiLCJ1cGxvYWRfZHVyYXRpb24iOiIwLjAwMDcyNjMzIn0sImlzcyI6ImdpdGxhYi13b3JraG9yc2UifQ.ZWd81CtJntE8ON61GVvlXGmkBk2FoVdGGRZafuBnb0Y&file.md5=3e242fb714d87b3c110c86476f5ff972&file.name=&file.path=%2FUsers%2Fdavid%2Fprojects%2Fgitlab-development-kit%2Fgitlab%2Fshared%2Fpackages%2Ftmp%2Fuploads%2F866554214&file.remote_id=&file.remote_url=&file.sha1=970318968feb640da723b8826861e41f0718a487&file.sha256=e49295702f7da8670778e9b95a281b72b41b31cb16afa376034b45f59a18ea3f&file.sha512=829056d4fd302cf2fdb43d9dcbb0a40cf0ccede72cd3fa47b420b15f4818c534004779b210d7b699866189b97fcdeb5c7c9c9fa3e2deccc3274b34e85ead3164&file.size=8&file.upload_duration=0.00072633
   ```
1. (Rails) _No object storage interactions_

Notes:
* The autorize request has a `Gitlab-Workhorse-Multipart-Fields` header. Q: Why do we need it? :thinking: multiple file uploads?
* The authorize response is a simple path. That's it.
* I'm wondering what would happen if we need to support multiple files. I guess this question has non sense in the Body Upload context. The body of the original request is the upload = one body, one file upload.
* The finalize request is a multipart request (content type `application/x-www-form-urlencoded`) and the file is passed in the "fixed" field `file`. This is confirmed by this [line](https://gitlab.com/gitlab-org/gitlab/-/blob/36253cf02090f65cc6e78411e865e8fb0896212d/workhorse/internal/upload/body_uploader.go#L81) in workhorse.
* Q: The finalize request is heavily signed. We can see JWT tokens flying around. Should responses coming from rails be signed too? Probably not.

#### Multipart Upload

1. (WH) Original request:
   ```
   PUT /api/v4/projects/509/packages/nuget/ HTTP/1.1
   Host: gdk.test:8000
   Transfer-Encoding: chunked
   Accept-Encoding: gzip, deflate
   Authorization: Basic <encoded PAT>
   Content-Type: multipart/form-data; boundary="9fa65681-2ee0-40b3-9c84-82f287e64844"
   User-Agent: NuGet xplat/5.8.0 (Darwin 20.6.0 Darwin Kernel Version 20.6.0: Mon Aug 30 06:12:21 PDT 2021; root:xnu-7195.141.6~3/RELEASE_X86_64)
   X-Nuget-Client-Version: 5.8.0
   X-Nuget-Session-Id: e9ec3499-35a6-4681-a9d7-8044fd862822
   
   10c6
   --9fa65681-2ee0-40b3-9c84-82f287e64844
   Content-Type: application/octet-stream
   Content-Disposition: form-data; name=package; filename=package.nupkg; filename*=utf-8''package.nupkg
   
   <gibberish file data>
   
   --9fa65681-2ee0-40b3-9c84-82f287e64844--
   0


   ```
1. (WH) Authorize request:
   ```
   PUT /api/v4/projects/509/packages/nuget/authorize HTTP/0.0
   Host: gdk.test:8000
   Authorization: Basic <encoded PAT>
   Gitlab-Workhorse: 11-10-0cfa69752d8-74ffd66ae-ee-133323-gf838263173a
   Gitlab-Workhorse-Api-Request: eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpc3MiOiJnaXRsYWItd29ya2hvcnNlIn0.y__tLHV0C92bicEJQZuGo0HGqbbJ0EbPY9Nv5bI0jKs
   User-Agent: NuGet xplat/5.8.0 (Darwin 20.6.0 Darwin Kernel Version 20.6.0: Mon Aug 30 06:12:21 PDT 2021; root:xnu-7195.141.6~3/RELEASE_X86_64)
   X-Nuget-Client-Version: 5.8.0
   X-Nuget-Session-Id: e9ec3499-35a6-4681-a9d7-8044fd862822
   ```
1. (Rails) _No object storage interactions_
1. (WH) Authorize response:
   ```
   HTTP/1.0 200 OK
   Connection: close
   Cache-Control: max-age=0, private, must-revalidate
   Content-Type: application/vnd.gitlab-workhorse+json
   Etag: W/"f7cd65573eb8ee1b6013569544aa589b"
   Vary: Origin
   X-Content-Type-Options: nosniff
   X-Frame-Options: SAMEORIGIN
   X-Request-Id: 01FPFSF9ETCVX78CNZKKES39P7
   X-Runtime: 0.478281
   
   {"TempPath":"/Users/david/projects/gitlab-development-kit/gitlab/shared/packages/tmp/uploads","MaximumSize":524288000}
   ```
1. (WH) Finalize request:
   ```
   PUT /api/v4/projects/509/packages/nuget/ HTTP/1.1
   Host: gdk.test:8000
   Transfer-Encoding: chunked
   Accept-Encoding: gzip, deflate
   Authorization: Basic <encoded PAT>
   Content-Type: multipart/form-data; boundary=6856383502371b7fcadd6867fd123cd1cc308c4422a457061c442142df48
   Gitlab-Workhorse: 11-10-0cfa69752d8-74ffd66ae-ee-133323-gf838263173a
   Gitlab-Workhorse-Api-Request: eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpc3MiOiJnaXRsYWItd29ya2hvcnNlIn0.y__tLHV0C92bicEJQZuGo0HGqbbJ0EbPY9Nv5bI0jKs
   Gitlab-Workhorse-Multipart-Fields: eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJyZXdyaXR0ZW5fZmllbGRzIjp7InBhY2thZ2UiOiIvVXNlcnMvZGF2aWQvcHJvamVjdHMvZ2l0bGFiLWRldmVsb3BtZW50LWtpdC9naXRsYWIvc2hhcmVkL3BhY2thZ2VzL3RtcC91cGxvYWRzL3BhY2thZ2UubnVwa2c1Mzg0NjE3NTMifSwiaXNzIjoiZ2l0bGFiLXdvcmtob3JzZSJ9.BIGDkCZ7U9Hcf5GtqYwBKH-VDxgakdgzjuqPJG0t9HM
   Gitlab-Workhorse-Proxy-Start: 1639060056071378000
   User-Agent: NuGet xplat/5.8.0 (Darwin 20.6.0 Darwin Kernel Version 20.6.0: Mon Aug 30 06:12:21 PDT 2021; root:xnu-7195.141.6~3/RELEASE_X86_64)
   X-Forwarded-For: 172.16.123.1
   X-Nuget-Client-Version: 5.8.0
   X-Nuget-Session-Id: e9ec3499-35a6-4681-a9d7-8044fd862822
   X-Sendfile-Type: X-Sendfile
   
   a62
   --6856383502371b7fcadd6867fd123cd1cc308c4422a457061c442142df48
   Content-Disposition: form-data; name="package.name"
   
   package.nupkg
   --6856383502371b7fcadd6867fd123cd1cc308c4422a457061c442142df48
   Content-Disposition: form-data; name="package.path"
   
   /Users/david/projects/gitlab-development-kit/gitlab/shared/packages/tmp/uploads/package.nupkg538461753
   --6856383502371b7fcadd6867fd123cd1cc308c4422a457061c442142df48
   Content-Disposition: form-data; name="package.remote_url"
   
   
   --6856383502371b7fcadd6867fd123cd1cc308c4422a457061c442142df48
   Content-Disposition: form-data; name="package.sha1"
   
   e58805a7673bf0f794e9e3c1add926088bbf19b7
   --6856383502371b7fcadd6867fd123cd1cc308c4422a457061c442142df48
   Content-Disposition: form-data; name="package.sha256"
   
   bac04410962a8c1721e409489b72ce38e4c47974bb4aa80b47a93daf51503e0c
   --6856383502371b7fcadd6867fd123cd1cc308c4422a457061c442142df48
   Content-Disposition: form-data; name="package.sha512"
   
   0edbffe3792aa38f314b4540aef60704f28c9611047c327a8cd8f9fa2446359736b9040d9b6e22e375f9da3a8b0c9f24c9c4e36f2e9995a3a15bfcad118238ea
   --6856383502371b7fcadd6867fd123cd1cc308c4422a457061c442142df48
   Content-Disposition: form-data; name="package.gitlab-workhorse-upload"
   
   eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1cGxvYWQiOnsibWQ1IjoiNjNkZTQ3ZGNkODhkNjBhYjU2MDZhMmU2MjliZjM5MzgiLCJuYW1lIjoicGFja2FnZS5udXBrZyIsInBhdGgiOiIvVXNlcnMvZGF2aWQvcHJvamVjdHMvZ2l0bGFiLWRldmVsb3BtZW50LWtpdC9naXRsYWIvc2hhcmVkL3BhY2thZ2VzL3RtcC91cGxvYWRzL3BhY2thZ2UubnVwa2c1Mzg0NjE3NTMiLCJyZW1vdGVfaWQiOiIiLCJyZW1vdGVfdXJsIjoiIiwic2hhMSI6ImU1ODgwNWE3NjczYmYwZjc5NGU5ZTNjMWFkZDkyNjA4OGJiZjE5YjciLCJzaGEyNTYiOiJiYWMwNDQxMDk2MmE4YzE3MjFlNDA5NDg5YjcyY2UzOGU0YzQ3OTc0YmI0YWE4MGI0N2E5M2RhZjUxNTAzZTBjIiwic2hhNTEyIjoiMGVkYmZmZTM3OTJhYTM4ZjMxNGI0NTQwYWVmNjA3MDRmMjhjOTYxMTA0N2MzMjdhOGNkOGY5ZmEyNDQ2MzU5NzM2YjkwNDBkOWI2ZTIyZTM3NWY5ZGEzYThiMGM5ZjI0YzljNGUzNmYyZTk5OTVhM2ExNWJmY2FkMTE4MjM4ZWEiLCJzaXplIjoiNDA2NiIsInVwbG9hZF9kdXJhdGlvbiI6IjAuMDAwNDg3ODg4In0sImlzcyI6ImdpdGxhYi13b3JraG9yc2UifQ.C1dWLpvdGcVm6TW9ExN7e2j9RLZgKIS4aceVvEmP6yc
   --6856383502371b7fcadd6867fd123cd1cc308c4422a457061c442142df48
   Content-Disposition: form-data; name="package.size"
   
   4066
   --6856383502371b7fcadd6867fd123cd1cc308c4422a457061c442142df48
   Content-Disposition: form-data; name="package.upload_duration"
   
   0.000487888
   --6856383502371b7fcadd6867fd123cd1cc308c4422a457061c442142df48
   Content-Disposition: form-data; name="package.remote_id"
   
   
   --6856383502371b7fcadd6867fd123cd1cc308c4422a457061c442142df48
   Content-Disposition: form-data; name="package.md5"
   
   63de47dcd88d60ab5606a2e629bf3938
   --6856383502371b7fcadd6867fd123cd1cc308c4422a457061c442142df48--
   
   0


   ```

* The authorize response has not indication of the file parameter (`package`). Q: multiple files uploaded with multipart encoded can't be authorized?
* The finalize request is also multipart but it's `multipart/form-data`. I don't know why? `application/x-www-form-urlencoded` could lead to a smaller payload but it's not that impactful. From my knowledge, `multipart/form-data` is used when you want to pass a file upload but with DirectUpload, the finalize call will never have a file, only references to a file somewhere = we can use the url encoded form.
   * Rails will read these in the same way.


### S3

The object storage is now provided by an S3 bucket.

#### Body Upload

1. (WH) Original request:
   ```
   PUT /api/v4/projects/509/packages/generic/my_package/0.0.1/file.txt HTTP/1.1
   Host: gdk.test:8000
   Accept: */*
   Content-Length: 8
   Expect: 100-continue
   Private-Token: <PAT>
   User-Agent: curl/7.64.1
   
   bananas
   
   ```
1. (WH) Authorize request:
   ```
   PUT /api/v4/projects/509/packages/generic/my_package/0.0.1/file.txt/authorize HTTP/0.0
   Host: gdk.test:8000
   Accept: */*
   Expect: 100-continue
   Gitlab-Workhorse: 11-10-0cfa69752d8-74ffd66ae-ee-133323-gf838263173a
   Gitlab-Workhorse-Api-Request: eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpc3MiOiJnaXRsYWItd29ya2hvcnNlIn0.y__tLHV0C92bicEJQZuGo0HGqbbJ0EbPY9Nv5bI0jKs
   Private-Token: <PAT>
   User-Agent: curl/7.64.1
   
   
   ```
1. (Rails) _No object storage interactions_
1. (WH) Authorize response:
   ```
   HTTP/1.0 200 OK
   Connection: close
   Cache-Control: max-age=0, private, must-revalidate
   Content-Type: application/vnd.gitlab-workhorse+json
   Etag: W/"a6be21b8704dcad6caf25a5f5727c131"
   Vary: Origin
   X-Content-Type-Options: nosniff
   X-Frame-Options: SAMEORIGIN
   X-Request-Id: 01FPFXF4FEG2XEQRKWE6GHVFB2
   X-Runtime: 0.790726
   
   {"RemoteObject":{"Timeout":14400,"GetURL":"http://127.0.0.1:9000/packages/tmp/uploads/1639064245-3480-0001-4657-9dbd7a2ee3eebcc73d6ea2e97e8bf30b?X-Amz-Expires=15300\u0026X-Amz-Date=20211209T153725Z\u0026X-Amz-Algorithm=AWS4-HMAC-SHA256\u0026X-Amz-Credential=minio%2F20211209%2Fgdk%2Fs3%2Faws4_request\u0026X-Amz-SignedHeaders=host\u0026X-Amz-Signature=59ec463ebf8bf15d1299f14779202200656042fa8e9e2f4d165eb4c1ef0114f7","StoreURL":"http://127.0.0.1:9000/packages/tmp/uploads/1639064245-3480-0001-4657-9dbd7a2ee3eebcc73d6ea2e97e8bf30b?X-Amz-Expires=15300\u0026X-Amz-Date=20211209T153725Z\u0026X-Amz-Algorithm=AWS4-HMAC-SHA256\u0026X-Amz-Credential=minio%2F20211209%2Fgdk%2Fs3%2Faws4_request\u0026X-Amz-SignedHeaders=host\u0026X-Amz-Signature=8aaa6d8d02306b4740cac08e1f293058d47e1f6ea83e14f1d31744a1221140f8","DeleteURL":"http://127.0.0.1:9000/packages/tmp/uploads/1639064245-3480-0001-4657-9dbd7a2ee3eebcc73d6ea2e97e8bf30b?X-Amz-Expires=15300\u0026X-Amz-Date=20211209T153725Z\u0026X-Amz-Algorithm=AWS4-HMAC-SHA256\u0026X-Amz-Credential=minio%2F20211209%2Fgdk%2Fs3%2Faws4_request\u0026X-Amz-SignedHeaders=host\u0026X-Amz-Signature=fead8096e8a37241226ea71431c555f1a572222a4c27046eee4439116ea65f1b","CustomPutHeaders":true,"PutHeaders":{},"UseWorkhorseClient":false,"RemoteTempObjectID":"tmp/uploads/1639064245-3480-0001-4657-9dbd7a2ee3eebcc73d6ea2e97e8bf30b","ObjectStorage":{"Provider":"AWS","S3Config":{"Bucket":"packages","Region":"gdk","Endpoint":"http://127.0.0.1:9000","PathStyle":true,"UseIamProfile":false}},"ID":"1639064245-3480-0001-4657-9dbd7a2ee3eebcc73d6ea2e97e8bf30b"}}
   ```
1. (WH) Finalize request:
   ```
   PUT /api/v4/projects/509/packages/generic/my_package/0.0.1/file.txt HTTP/1.1
   Host: gdk.test:8000
   Accept: */*
   Content-Length: 8
   Content-Type: application/x-www-form-urlencoded
   Expect: 100-continue
   Gitlab-Workhorse-Multipart-Fields: eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJyZXdyaXR0ZW5fZmllbGRzIjp7ImZpbGUiOiIifSwiaXNzIjoiZ2l0bGFiLXdvcmtob3JzZSJ9.k4zvceZKFj4EehpnHsyMUfOZtBb1N60IkyXaeqV0CSA
   Private-Token: <PAT>
   User-Agent: curl/7.64.1
   
   file.gitlab-workhorse-upload=eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1cGxvYWQiOnsibWQ1IjoiM2UyNDJmYjcxNGQ4N2IzYzExMGM4NjQ3NmY1ZmY5NzIiLCJuYW1lIjoiIiwicGF0aCI6IiIsInJlbW90ZV9pZCI6IjE2Mzk3NTIzNjMtMjE2NzAtMDAwMy01MjM2LWY1MzVkZjk3ZjE1NTA0ZjkxZTc4MTM4ZjQxNjg2YmU2IiwicmVtb3RlX3VybCI6Imh0dHA6Ly8xMjcuMC4wLjE6OTAwMC9wYWNrYWdlcy90bXAvdXBsb2Fkcy8xNjM5NzUyMzYzLTIxNjcwLTAwMDMtNTIzNi1mNTM1ZGY5N2YxNTUwNGY5MWU3ODEzOGY0MTY4NmJlNj9YLUFtei1FeHBpcmVzPTE1MzAwXHUwMDI2WC1BbXotRGF0ZT0yMDIxMTIxN1QxNDQ2MDNaXHUwMDI2WC1BbXotQWxnb3JpdGhtPUFXUzQtSE1BQy1TSEEyNTZcdTAwMjZYLUFtei1DcmVkZW50aWFsPW1pbmlvJTJGMjAyMTEyMTclMkZnZGslMkZzMyUyRmF3czRfcmVxdWVzdFx1MDAyNlgtQW16LVNpZ25lZEhlYWRlcnM9aG9zdFx1MDAyNlgtQW16LVNpZ25hdHVyZT0zNjdjNmZmZGZiZjNhNGU5NGUzMDUwZjkzOWI2ZmNhOTNiYjc5NzhiMzNmMWMzOTQyNTMyN2E3NDljOGZlNzZmIiwic2hhMSI6Ijk3MDMxODk2OGZlYjY0MGRhNzIzYjg4MjY4NjFlNDFmMDcxOGE0ODciLCJzaGEyNTYiOiJlNDkyOTU3MDJmN2RhODY3MDc3OGU5Yjk1YTI4MWI3MmI0MWIzMWNiMTZhZmEzNzYwMzRiNDVmNTlhMThlYTNmIiwic2hhNTEyIjoiODI5MDU2ZDRmZDMwMmNmMmZkYjQzZDlkY2JiMGE0MGNmMGNjZWRlNzJjZDNmYTQ3YjQyMGIxNWY0ODE4YzUzNDAwNDc3OWIyMTBkN2I2OTk4NjYxODliOTdmY2RlYjVjN2M5YzlmYTNlMmRlY2NjMzI3NGIzNGU4NWVhZDMxNjQiLCJzaXplIjoiOCIsInVwbG9hZF9kdXJhdGlvbiI6IjAuMDA3NjU1OTkifSwiaXNzIjoiZ2l0bGFiLXdvcmtob3JzZSJ9.jaCjJ6W-kOHMJz38z1M6MCSHLuu2SSricvQJgvwzd0Y&file.md5=3e242fb714d87b3c110c86476f5ff972&file.name=&file.path=&file.remote_id=1639064826-3480-0002-1297-17d217dbb1a47afd165cd508ffb815396&file.remote_url=http%3A%2F%2F127.0.0.1%3A9000%2Fpackages%2Ftmp%2Fuploads%2F1639064826-3480-0002-1297-17d217dbb1a47afd165cd508ffb815396%3FX-Amz-Expires%3D15300%26X-Amz-Date%3D20211217T144603Z%26X-Amz-Algorithm%3DAWS4-HMAC-SHA256%26X-Amz-Credential%3Dminio%252F20211217%252Fgdk%252Fs3%252Faws4_request%26X-Amz-SignedHeaders%3Dhost%26X-Amz-Signature%3D367c6ffdfbf3a4e94e3050f939b6fca93bb7978b33f1c39425327a749c8fe76f&file.sha1=970318968feb640da723b8826861e41f0718a487&file.sha256=e49295702f7da8670778e9b95a281b72b41b31cb16afa376034b45f59a18ea3f&file.sha512=829056d4fd302cf2fdb43d9dcbb0a40cf0ccede72cd3fa47b420b15f4818c534004779b210d7b699866189b97fcdeb5c7c9c9fa3e2deccc3274b34e85ead3164&file.size=8&file.upload_duration=0.00765599

   ```
1. (Rails) Object storage interactions:
   ```
   HEAD http://127.0.0.1:9000/packages/tmp/uploads/1639064826-3480-0002-1297-17d217dbb1a47afd165cd508ffb81539
   PUT http://127.0.0.1:9000/packages/a0/51/a05198938c6ca8cd56289c6dba6bb8aaa68dfe8e0d7a37df2fb76e48eeba4244/packages/791/files/1290/file.txt (with x-amz-copy-source)
   HEAD http://127.0.0.1:9000/packages/a0/51/a05198938c6ca8cd56289c6dba6bb8aaa68dfe8e0d7a37df2fb76e48eeba4244/packages/791/files/1290/file.txt
   DELETE http://127.0.0.1:9000/packages/tmp/uploads/1639064826-3480-0002-1297-17d217dbb1a47afd165cd508ffb81539
   ```
* The Authorize response is quite complex. It has all the presigned URLs needed to manipulate the target object storage location. The target location is a temporary one.
* The Authorize response has the object storage configuration but it's not really needed as in this case, we have pre signed urls (this is from non consolidate form)
* The Finalize request has a `remote_url`. Why do we need that? We only need to object storage location and that's it.
* The Finalize request is using `application/x-www-form-urlencoded`
* After the finalize request, Rails do a copy of the object (from the temporary key to the final key) and destroy the temporary key. Q: Why do we need this? Can't the Authorize response point to the final location already?

#### Multipart Upload

1. (WH) Original request:
   ```
   PUT /api/v4/projects/509/packages/nuget/ HTTP/1.1
   Host: gdk.test:8000
   Transfer-Encoding: chunked
   Accept-Encoding: gzip, deflate
   Authorization: Basic <encoded PAT>
   Content-Type: multipart/form-data; boundary="aecc6dc7-98b3-470e-af50-9c756ad1e796"
   User-Agent: NuGet xplat/5.8.0 (Darwin 20.6.0 Darwin Kernel Version 20.6.0: Mon Aug 30 06:12:21 PDT 2021; root:xnu-7195.141.6~3/RELEASE_X86_64)
   X-Nuget-Client-Version: 5.8.0
   X-Nuget-Session-Id: 53de9701-6161-4942-82e2-c07f289f691c
   
   10c7
   --aecc6dc7-98b3-470e-af50-9c756ad1e796
   Content-Type: application/octet-stream
   Content-Disposition: form-data; name=package; filename=package.nupkg; filename*=utf-8''package.nupkg
   
   <gibberish file data>
   --aecc6dc7-98b3-470e-af50-9c756ad1e796--
   
   0
   
   
   ```
1. (WH) Authorize request:
   ```
   PUT /api/v4/projects/509/packages/nuget/authorize HTTP/0.0
   Host: gdk.test:8000
   Authorization: Basic <PAT>
   Gitlab-Workhorse: 11-10-0cfa69752d8-74ffd66ae-ee-133323-gf838263173a
   Gitlab-Workhorse-Api-Request: eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpc3MiOiJnaXRsYWItd29ya2hvcnNlIn0.y__tLHV0C92bicEJQZuGo0HGqbbJ0EbPY9Nv5bI0jKs
   User-Agent: NuGet xplat/5.8.0 (Darwin 20.6.0 Darwin Kernel Version 20.6.0: Mon Aug 30 06:12:21 PDT 2021; root:xnu-7195.141.6~3/RELEASE_X86_64)
   X-Nuget-Client-Version: 5.8.0
   X-Nuget-Session-Id: 53de9701-6161-4942-82e2-c07f289f691c
   
   
   ```
1. (Rails) _No object storage interactions_
1. (WH) Authorize response:
   ```
   HTTP/1.0 200 OK
   Connection: close
   Cache-Control: max-age=0, private, must-revalidate
   Content-Type: application/vnd.gitlab-workhorse+json
   Etag: W/"f0ee3a38a478fd06bfb67e7784433405"
   Vary: Origin
   X-Content-Type-Options: nosniff
   X-Frame-Options: SAMEORIGIN
   X-Request-Id: 01FPFYJYQZ35BH2TWSTVTX7VES
   X-Runtime: 0.441594
   
   {"RemoteObject":{"Timeout":14400,"GetURL":"http://127.0.0.1:9000/packages/tmp/uploads/1639751506-21670-0002-9628-c1050a94854bc6fb445cd2b2e4ee543b?X-Amz-Expires=15300\u0026X-Amz-Date=20211209T155658Z\u0026X-Amz-Algorithm=AWS4-HMAC-SHA256\u0026X-Amz-Credential=minio%2F20211209%2Fgdk%2Fs3%2Faws4_request\u0026X-Amz-SignedHeaders=host\u0026X-Amz-Signature=ee239ccfcd3f3c8f084d5ea93524efd56060f3b50ef12413535c189a3e6ff66d","StoreURL":"http://127.0.0.1:9000/packages/tmp/uploads/1639751506-21670-0002-9628-c1050a94854bc6fb445cd2b2e4ee543b?X-Amz-Expires=15300\u0026X-Amz-Date=20211209T155658Z\u0026X-Amz-Algorithm=AWS4-HMAC-SHA256\u0026X-Amz-Credential=minio%2F20211209%2Fgdk%2Fs3%2Faws4_request\u0026X-Amz-SignedHeaders=host\u0026X-Amz-Signature=bd2ca4d945ba882452c34ff2c1d093b8310af1c2cb094b3aae1e05e507afd879","DeleteURL":"http://127.0.0.1:9000/packages/tmp/uploads/1639751506-21670-0002-9628-c1050a94854bc6fb445cd2b2e4ee543b?X-Amz-Expires=15300\u0026X-Amz-Date=20211209T155658Z\u0026X-Amz-Algorithm=AWS4-HMAC-SHA256\u0026X-Amz-Credential=minio%2F20211209%2Fgdk%2Fs3%2Faws4_request\u0026X-Amz-SignedHeaders=host\u0026X-Amz-Signature=3a68d53d5be25817fe626434781c1671337766b9fcbd7aa34ca966dce62b34f9","MultipartUpload":{"PartSize":5242880,"PartURLs":["http://127.0.0.1:9000/packages/tmp/uploads/1639751506-21670-0002-9628-c1050a94854bc6fb445cd2b2e4ee543b?uploadId=02f158fb-ac7e-4c85-8d04-ae116839c24e\u0026partNumber=1\u0026X-Amz-Expires=15300\u0026X-Amz-Date=20211209T155658Z\u0026X-Amz-Algorithm=AWS4-HMAC-SHA256\u0026X-Amz-Credential=minio%2F20211209%2Fgdk%2Fs3%2Faws4_request\u0026X-Amz-SignedHeaders=host\u0026X-Amz-Signature=f2f7c6ca5e1b7b5e416c06e80653bcb7405109fc599aeb231e77f93866c463f0","http://127.0.0.1:9000/packages/tmp/uploads/1639751506-21670-0002-9628-c1050a94854bc6fb445cd2b2e4ee543b?uploadId=02f158fb-ac7e-4c85-8d04-ae116839c24e\u0026partNumber=2\u0026X-Amz-Expires=15300\u0026X-Amz-Date=20211209T155658Z\u0026X-Amz-Algorithm=AWS4-HMAC-SHA256\u0026X-Amz-Credential=minio%2F20211209%2Fgdk%2Fs3%2Faws4_request\u0026X-Amz-SignedHeaders=host\u0026X-Amz-Signature=14f10910f001bcbfa24755ba676bc6221bfab42a13ffb9b802fac9b256767849","http://127.0.0.1:9000/packages/tmp/uploads/1639751506-21670-0002-9628-c1050a94854bc6fb445cd2b2e4ee543b?uploadId=02f158fb-ac7e-4c85-8d04-ae116839c24e\u0026partNumber=3\u0026X-Amz-Expires=15300\u0026X-Amz-Date=20211209T155658Z\u0026X-Amz-Algorithm=AWS4-HMAC-SHA256\u0026X-Amz-Credential=minio%2F20211209%2Fgdk%2Fs3%2Faws4_request\u0026X-Amz-SignedHeaders=host\u0026X-Amz-Signature=917c28acbca1e64ed9f50b5b229f88bc9bf53f0373294df849fb9a5ade7cdda5","http://127.0.0.1:9000/packages/tmp/uploads/1639751506-21670-0002-9628-c1050a94854bc6fb445cd2b2e4ee543b?uploadId=02f158fb-ac7e-4c85-8d04-ae116839c24e\u0026partNumber=4\u0026X-Amz-Expires=15300\u0026X-Amz-Date=20211209T155658Z\u0026X-Amz-Algorithm=AWS4-HMAC-SHA256\u0026X-Amz-Credential=minio%2F20211209%2Fgdk%2Fs3%2Faws4_request\u0026X-Amz-SignedHeaders=host\u0026X-Amz-Signature=9a8a809aa677ce38f05499c66c695393748384df2afd2e4b993f7bad34213d83","http://127.0.0.1:9000/packages/tmp/uploads/1639751506-21670-0002-9628-c1050a94854bc6fb445cd2b2e4ee543b?uploadId=02f158fb-ac7e-4c85-8d04-ae116839c24e\u0026partNumber=5\u0026X-Amz-Expires=15300\u0026X-Amz-Date=20211209T155658Z\u0026X-Amz-Algorithm=AWS4-HMAC-SHA256\u0026X-Amz-Credential=minio%2F20211209%2Fgdk%2Fs3%2Faws4_request\u0026X-Amz-SignedHeaders=host\u0026X-Amz-Signature=476f2b723d6c6e07e9b1946defd0ef7190e8b01db7352191c8f6e977496b795b","http://127.0.0.1:9000/packages/tmp/uploads/1639751506-21670-0002-9628-c1050a94854bc6fb445cd2b2e4ee543b?uploadId=02f158fb-ac7e-4c85-8d04-ae116839c24e\u0026partNumber=6\u0026X-Amz-Expires=15300\u0026X-Amz-Date=20211209T155658Z\u0026X-Amz-Algorithm=AWS4-HMAC-SHA256\u0026X-Amz-Credential=minio%2F20211209%2Fgdk%2Fs3%2Faws4_request\u0026X-Amz-SignedHeaders=host\u0026X-Amz-Signature=25f76eba9073bf865131afdf97c72fc27cf20ae4fad925a3e2c84dcb83533991","http://127.0.0.1:9000/packages/tmp/uploads/1639751506-21670-0002-9628-c1050a94854bc6fb445cd2b2e4ee543b?uploadId=02f158fb-ac7e-4c85-8d04-ae116839c24e\u0026partNumber=7\u0026X-Amz-Expires=15300\u0026X-Amz-Date=20211209T155658Z\u0026X-Amz-Algorithm=AWS4-HMAC-SHA256\u0026X-Amz-Credential=minio%2F20211209%2Fgdk%2Fs3%2Faws4_request\u0026X-Amz-SignedHeaders=host\u0026X-Amz-Signature=f8a1c4a352840b4045e0e1e5b7d29691cab9989bd8d750a17b66ec7ae69e55e8","http://127.0.0.1:9000/packages/tmp/uploads/1639751506-21670-0002-9628-c1050a94854bc6fb445cd2b2e4ee543b?uploadId=02f158fb-ac7e-4c85-8d04-ae116839c24e\u0026partNumber=8\u0026X-Amz-Expires=15300\u0026X-Amz-Date=20211209T155658Z\u0026X-Amz-Algorithm=AWS4-HMAC-SHA256\u0026X-Amz-Credential=minio%2F20211209%2Fgdk%2Fs3%2Faws4_request\u0026X-Amz-SignedHeaders=host\u0026X-Amz-Signature=56ad3e3c401ce6b9df511f5be1ce79379c87710804b8871885373b05bb3d5ccc","http://127.0.0.1:9000/packages/tmp/uploads/1639751506-21670-0002-9628-c1050a94854bc6fb445cd2b2e4ee543b?uploadId=02f158fb-ac7e-4c85-8d04-ae116839c24e\u0026partNumber=9\u0026X-Amz-Expires=15300\u0026X-Amz-Date=20211209T155658Z\u0026X-Amz-Algorithm=AWS4-HMAC-SHA256\u0026X-Amz-Credential=minio%2F20211209%2Fgdk%2Fs3%2Faws4_request\u0026X-Amz-SignedHeaders=host\u0026X-Amz-Signature=25d3482b1748f7d0b934b1fa93461e06100ca8c678f720aae54da83a65878d73","http://127.0.0.1:9000/packages/tmp/uploads/1639751506-21670-0002-9628-c1050a94854bc6fb445cd2b2e4ee543b?uploadId=02f158fb-ac7e-4c85-8d04-ae116839c24e\u0026partNumber=10\u0026X-Amz-Expires=15300\u0026X-Amz-Date=20211209T155658Z\u0026X-Amz-Algorithm=AWS4-HMAC-SHA256\u0026X-Amz-Credential=minio%2F20211209%2Fgdk%2Fs3%2Faws4_request\u0026X-Amz-SignedHeaders=host\u0026X-Amz-Signature=e5ebb846c8e9b19b8b89de30c58a9118d23198d68bafad2e7fa98dd57dcb7164","http://127.0.0.1:9000/packages/tmp/uploads/1639751506-21670-0002-9628-c1050a94854bc6fb445cd2b2e4ee543b?uploadId=02f158fb-ac7e-4c85-8d04-ae116839c24e\u0026partNumber=11\u0026X-Amz-Expires=15300\u0026X-Amz-Date=20211209T155658Z\u0026X-Amz-Algorithm=AWS4-HMAC-SHA256\u0026X-Amz-Credential=minio%2F20211209%2Fgdk%2Fs3%2Faws4_request\u0026X-Amz-SignedHeaders=host\u0026X-Amz-Signature=bb4d607e5b324960e5f589223cb31f5fbdf4f68247d7402022e25062da7758d6","http://127.0.0.1:9000/packages/tmp/uploads/1639751506-21670-0002-9628-c1050a94854bc6fb445cd2b2e4ee543b?uploadId=02f158fb-ac7e-4c85-8d04-ae116839c24e\u0026partNumber=12\u0026X-Amz-Expires=15300\u0026X-Amz-Date=20211209T155658Z\u0026X-Amz-Algorithm=AWS4-HMAC-SHA256\u0026X-Amz-Credential=minio%2F20211209%2Fgdk%2Fs3%2Faws4_request\u0026X-Amz-SignedHeaders=host\u0026X-Amz-Signature=8669331a0898944298e91b206a2e5a42a0e6f33ac8cfc52ae0f934efbf38c7c9","http://127.0.0.1:9000/packages/tmp/uploads/1639751506-21670-0002-9628-c1050a94854bc6fb445cd2b2e4ee543b?uploadId=02f158fb-ac7e-4c85-8d04-ae116839c24e\u0026partNumber=13\u0026X-Amz-Expires=15300\u0026X-Amz-Date=20211209T155658Z\u0026X-Amz-Algorithm=AWS4-HMAC-SHA256\u0026X-Amz-Credential=minio%2F20211209%2Fgdk%2Fs3%2Faws4_request\u0026X-Amz-SignedHeaders=host\u0026X-Amz-Signature=825454f239bdd002db65eceb354ce39242a54a31f15862afc18abefba72debc6","http://127.0.0.1:9000/packages/tmp/uploads/1639751506-21670-0002-9628-c1050a94854bc6fb445cd2b2e4ee543b?uploadId=02f158fb-ac7e-4c85-8d04-ae116839c24e\u0026partNumber=14\u0026X-Amz-Expires=15300\u0026X-Amz-Date=20211209T155658Z\u0026X-Amz-Algorithm=AWS4-HMAC-SHA256\u0026X-Amz-Credential=minio%2F20211209%2Fgdk%2Fs3%2Faws4_request\u0026X-Amz-SignedHeaders=host\u0026X-Amz-Signature=56d3f8b8a998f2c6e56b1da8d055340c59d5833070242a5f04b4ef03afd604f3","http://127.0.0.1:9000/packages/tmp/uploads/1639751506-21670-0002-9628-c1050a94854bc6fb445cd2b2e4ee543b?uploadId=02f158fb-ac7e-4c85-8d04-ae116839c24e\u0026partNumber=15\u0026X-Amz-Expires=15300\u0026X-Amz-Date=20211209T155658Z\u0026X-Amz-Algorithm=AWS4-HMAC-SHA256\u0026X-Amz-Credential=minio%2F20211209%2Fgdk%2Fs3%2Faws4_request\u0026X-Amz-SignedHeaders=host\u0026X-Amz-Signature=76e6813d92781c48e4cc064e7ca0910ab5cedf479d9b4feaa856c765d14cd82a","http://127.0.0.1:9000/packages/tmp/uploads/1639751506-21670-0002-9628-c1050a94854bc6fb445cd2b2e4ee543b?uploadId=02f158fb-ac7e-4c85-8d04-ae116839c24e\u0026partNumber=16\u0026X-Amz-Expires=15300\u0026X-Amz-Date=20211209T155658Z\u0026X-Amz-Algorithm=AWS4-HMAC-SHA256\u0026X-Amz-Credential=minio%2F20211209%2Fgdk%2Fs3%2Faws4_request\u0026X-Amz-SignedHeaders=host\u0026X-Amz-Signature=797889fed5465ff3eac69fab70d0d3008425eb47831aa30f325e8acaa7007609","http://127.0.0.1:9000/packages/tmp/uploads/1639751506-21670-0002-9628-c1050a94854bc6fb445cd2b2e4ee543b?uploadId=02f158fb-ac7e-4c85-8d04-ae116839c24e\u0026partNumber=17\u0026X-Amz-Expires=15300\u0026X-Amz-Date=20211209T155658Z\u0026X-Amz-Algorithm=AWS4-HMAC-SHA256\u0026X-Amz-Credential=minio%2F20211209%2Fgdk%2Fs3%2Faws4_request\u0026X-Amz-SignedHeaders=host\u0026X-Amz-Signature=d9cc79820b70c1525109027488137ce45ec0533763aed4de28c0ba7765cb06d7","http://127.0.0.1:9000/packages/tmp/uploads/1639751506-21670-0002-9628-c1050a94854bc6fb445cd2b2e4ee543b?uploadId=02f158fb-ac7e-4c85-8d04-ae116839c24e\u0026partNumber=18\u0026X-Amz-Expires=15300\u0026X-Amz-Date=20211209T155658Z\u0026X-Amz-Algorithm=AWS4-HMAC-SHA256\u0026X-Amz-Credential=minio%2F20211209%2Fgdk%2Fs3%2Faws4_request\u0026X-Amz-SignedHeaders=host\u0026X-Amz-Signature=be764146b0a213d23d2761b5aba09579c03d0c36c3028dda6903b87e45e6a6ce","http://127.0.0.1:9000/packages/tmp/uploads/1639751506-21670-0002-9628-c1050a94854bc6fb445cd2b2e4ee543b?uploadId=02f158fb-ac7e-4c85-8d04-ae116839c24e\u0026partNumber=19\u0026X-Amz-Expires=15300\u0026X-Amz-Date=20211209T155658Z\u0026X-Amz-Algorithm=AWS4-HMAC-SHA256\u0026X-Amz-Credential=minio%2F20211209%2Fgdk%2Fs3%2Faws4_request\u0026X-Amz-SignedHeaders=host\u0026X-Amz-Signature=c9dca34b6abf8222bf44c74134afbdb2e190fac2cb1fc04b8b1b1de63e4c5109","http://127.0.0.1:9000/packages/tmp/uploads/1639751506-21670-0002-9628-c1050a94854bc6fb445cd2b2e4ee543b?uploadId=02f158fb-ac7e-4c85-8d04-ae116839c24e\u0026partNumber=20\u0026X-Amz-Expires=15300\u0026X-Amz-Date=20211209T155658Z\u0026X-Amz-Algorithm=AWS4-HMAC-SHA256\u0026X-Amz-Credential=minio%2F20211209%2Fgdk%2Fs3%2Faws4_request\u0026X-Amz-SignedHeaders=host\u0026X-Amz-Signature=331197c9f619ed3dffed7bcad71ea91e5d0b0483bea853af9b50db9b34a74557","http://127.0.0.1:9000/packages/tmp/uploads/1639751506-21670-0002-9628-c1050a94854bc6fb445cd2b2e4ee543b?uploadId=02f158fb-ac7e-4c85-8d04-ae116839c24e\u0026partNumber=21\u0026X-Amz-Expires=15300\u0026X-Amz-Date=20211209T155658Z\u0026X-Amz-Algorithm=AWS4-HMAC-SHA256\u0026X-Amz-Credential=minio%2F20211209%2Fgdk%2Fs3%2Faws4_request\u0026X-Amz-SignedHeaders=host\u0026X-Amz-Signature=c66395cc6ad7ebdab4c5dbf89ae8aa74db77b66729fc952d380826992d2c3aa7","http://127.0.0.1:9000/packages/tmp/uploads/1639751506-21670-0002-9628-c1050a94854bc6fb445cd2b2e4ee543b?uploadId=02f158fb-ac7e-4c85-8d04-ae116839c24e\u0026partNumber=22\u0026X-Amz-Expires=15300\u0026X-Amz-Date=20211209T155658Z\u0026X-Amz-Algorithm=AWS4-HMAC-SHA256\u0026X-Amz-Credential=minio%2F20211209%2Fgdk%2Fs3%2Faws4_request\u0026X-Amz-SignedHeaders=host\u0026X-Amz-Signature=c2943d50835cdfd2d6ea1ad15680cb28edf308f0db6e15ce6a0e7b2e8b2067cb","http://127.0.0.1:9000/packages/tmp/uploads/1639751506-21670-0002-9628-c1050a94854bc6fb445cd2b2e4ee543b?uploadId=02f158fb-ac7e-4c85-8d04-ae116839c24e\u0026partNumber=23\u0026X-Amz-Expires=15300\u0026X-Amz-Date=20211209T155658Z\u0026X-Amz-Algorithm=AWS4-HMAC-SHA256\u0026X-Amz-Credential=minio%2F20211209%2Fgdk%2Fs3%2Faws4_request\u0026X-Amz-SignedHeaders=host\u0026X-Amz-Signature=9a7f3bd96b84635fb2356ca92c34da9f36ba7b896135937282109b386f318cb9","http://127.0.0.1:9000/packages/tmp/uploads/1639751506-21670-0002-9628-c1050a94854bc6fb445cd2b2e4ee543b?uploadId=02f158fb-ac7e-4c85-8d04-ae116839c24e\u0026partNumber=24\u0026X-Amz-Expires=15300\u0026X-Amz-Date=20211209T155658Z\u0026X-Amz-Algorithm=AWS4-HMAC-SHA256\u0026X-Amz-Credential=minio%2F20211209%2Fgdk%2Fs3%2Faws4_request\u0026X-Amz-SignedHeaders=host\u0026X-Amz-Signature=c376cc0bec074118c2b32481d0e2d226575d490d5665575c1f6789848fc716cd","http://127.0.0.1:9000/packages/tmp/uploads/1639751506-21670-0002-9628-c1050a94854bc6fb445cd2b2e4ee543b?uploadId=02f158fb-ac7e-4c85-8d04-ae116839c24e\u0026partNumber=25\u0026X-Amz-Expires=15300\u0026X-Amz-Date=20211209T155658Z\u0026X-Amz-Algorithm=AWS4-HMAC-SHA256\u0026X-Amz-Credential=minio%2F20211209%2Fgdk%2Fs3%2Faws4_request\u0026X-Amz-SignedHeaders=host\u0026X-Amz-Signature=ec0cc8136ad9d528785745c44e861631d3982f207863979ec3bdf59efb111b63","http://127.0.0.1:9000/packages/tmp/uploads/1639751506-21670-0002-9628-c1050a94854bc6fb445cd2b2e4ee543b?uploadId=02f158fb-ac7e-4c85-8d04-ae116839c24e\u0026partNumber=26\u0026X-Amz-Expires=15300\u0026X-Amz-Date=20211209T155658Z\u0026X-Amz-Algorithm=AWS4-HMAC-SHA256\u0026X-Amz-Credential=minio%2F20211209%2Fgdk%2Fs3%2Faws4_request\u0026X-Amz-SignedHeaders=host\u0026X-Amz-Signature=e5cdb48b86777ddba24f729baa238ae673d5cfd2d1d168e5a61b9e40d80fec62","http://127.0.0.1:9000/packages/tmp/uploads/1639751506-21670-0002-9628-c1050a94854bc6fb445cd2b2e4ee543b?uploadId=02f158fb-ac7e-4c85-8d04-ae116839c24e\u0026partNumber=27\u0026X-Amz-Expires=15300\u0026X-Amz-Date=20211209T155658Z\u0026X-Amz-Algorithm=AWS4-HMAC-SHA256\u0026X-Amz-Credential=minio%2F20211209%2Fgdk%2Fs3%2Faws4_request\u0026X-Amz-SignedHeaders=host\u0026X-Amz-Signature=35d09103a437c82a99bf7531b6388871ee87b4917d1cf53808be0acb83cf7a22","http://127.0.0.1:9000/packages/tmp/uploads/1639751506-21670-0002-9628-c1050a94854bc6fb445cd2b2e4ee543b?uploadId=02f158fb-ac7e-4c85-8d04-ae116839c24e\u0026partNumber=28\u0026X-Amz-Expires=15300\u0026X-Amz-Date=20211209T155658Z\u0026X-Amz-Algorithm=AWS4-HMAC-SHA256\u0026X-Amz-Credential=minio%2F20211209%2Fgdk%2Fs3%2Faws4_request\u0026X-Amz-SignedHeaders=host\u0026X-Amz-Signature=454b537e4fae11a61b20c3af3e156883b515df572c67aa3ae4895b7fa919812a","http://127.0.0.1:9000/packages/tmp/uploads/1639751506-21670-0002-9628-c1050a94854bc6fb445cd2b2e4ee543b?uploadId=02f158fb-ac7e-4c85-8d04-ae116839c24e\u0026partNumber=29\u0026X-Amz-Expires=15300\u0026X-Amz-Date=20211209T155658Z\u0026X-Amz-Algorithm=AWS4-HMAC-SHA256\u0026X-Amz-Credential=minio%2F20211209%2Fgdk%2Fs3%2Faws4_request\u0026X-Amz-SignedHeaders=host\u0026X-Amz-Signature=8b469cffe019ad034c5d625508167c2d6e3b1c60008c3bb7a64e15b48cd712d1","http://127.0.0.1:9000/packages/tmp/uploads/1639751506-21670-0002-9628-c1050a94854bc6fb445cd2b2e4ee543b?uploadId=02f158fb-ac7e-4c85-8d04-ae116839c24e\u0026partNumber=30\u0026X-Amz-Expires=15300\u0026X-Amz-Date=20211209T155658Z\u0026X-Amz-Algorithm=AWS4-HMAC-SHA256\u0026X-Amz-Credential=minio%2F20211209%2Fgdk%2Fs3%2Faws4_request\u0026X-Amz-SignedHeaders=host\u0026X-Amz-Signature=8bd20bfbae8b39cc1ce1f134361218cbb40083ad36a89ba7b04b552a383f41e9","http://127.0.0.1:9000/packages/tmp/uploads/1639751506-21670-0002-9628-c1050a94854bc6fb445cd2b2e4ee543b?uploadId=02f158fb-ac7e-4c85-8d04-ae116839c24e\u0026partNumber=31\u0026X-Amz-Expires=15300\u0026X-Amz-Date=20211209T155658Z\u0026X-Amz-Algorithm=AWS4-HMAC-SHA256\u0026X-Amz-Credential=minio%2F20211209%2Fgdk%2Fs3%2Faws4_request\u0026X-Amz-SignedHeaders=host\u0026X-Amz-Signature=bedb6109815047cbbe51d79804ac1f55ca8549ae076b9f34c8740f8f5b41b821","http://127.0.0.1:9000/packages/tmp/uploads/1639751506-21670-0002-9628-c1050a94854bc6fb445cd2b2e4ee543b?uploadId=02f158fb-ac7e-4c85-8d04-ae116839c24e\u0026partNumber=32\u0026X-Amz-Expires=15300\u0026X-Amz-Date=20211209T155658Z\u0026X-Amz-Algorithm=AWS4-HMAC-SHA256\u0026X-Amz-Credential=minio%2F20211209%2Fgdk%2Fs3%2Faws4_request\u0026X-Amz-SignedHeaders=host\u0026X-Amz-Signature=bddc188e79e65b8a913803e5c772b95b936b53648cc721f7af04e383e19cbb40","http://127.0.0.1:9000/packages/tmp/uploads/1639751506-21670-0002-9628-c1050a94854bc6fb445cd2b2e4ee543b?uploadId=02f158fb-ac7e-4c85-8d04-ae116839c24e\u0026partNumber=33\u0026X-Amz-Expires=15300\u0026X-Amz-Date=20211209T155658Z\u0026X-Amz-Algorithm=AWS4-HMAC-SHA256\u0026X-Amz-Credential=minio%2F20211209%2Fgdk%2Fs3%2Faws4_request\u0026X-Amz-SignedHeaders=host\u0026X-Amz-Signature=4c722ee11fcae5a9b87c834cdeca9f0c232ee57cbc596a5dbb387a47f1f039ce","http://127.0.0.1:9000/packages/tmp/uploads/1639751506-21670-0002-9628-c1050a94854bc6fb445cd2b2e4ee543b?uploadId=02f158fb-ac7e-4c85-8d04-ae116839c24e\u0026partNumber=34\u0026X-Amz-Expires=15300\u0026X-Amz-Date=20211209T155658Z\u0026X-Amz-Algorithm=AWS4-HMAC-SHA256\u0026X-Amz-Credential=minio%2F20211209%2Fgdk%2Fs3%2Faws4_request\u0026X-Amz-SignedHeaders=host\u0026X-Amz-Signature=5ac48dd89f1942483254f24877eb0657fc3d6e85932b8e13b31aa313a88578f4","http://127.0.0.1:9000/packages/tmp/uploads/1639751506-21670-0002-9628-c1050a94854bc6fb445cd2b2e4ee543b?uploadId=02f158fb-ac7e-4c85-8d04-ae116839c24e\u0026partNumber=35\u0026X-Amz-Expires=15300\u0026X-Amz-Date=20211209T155658Z\u0026X-Amz-Algorithm=AWS4-HMAC-SHA256\u0026X-Amz-Credential=minio%2F20211209%2Fgdk%2Fs3%2Faws4_request\u0026X-Amz-SignedHeaders=host\u0026X-Amz-Signature=a8d779bd3b8c6f18aa1a2f73abde757ac0ee2ee8607cd6272bb666c80912ef0f","http://127.0.0.1:9000/packages/tmp/uploads/1639751506-21670-0002-9628-c1050a94854bc6fb445cd2b2e4ee543b?uploadId=02f158fb-ac7e-4c85-8d04-ae116839c24e\u0026partNumber=36\u0026X-Amz-Expires=15300\u0026X-Amz-Date=20211209T155658Z\u0026X-Amz-Algorithm=AWS4-HMAC-SHA256\u0026X-Amz-Credential=minio%2F20211209%2Fgdk%2Fs3%2Faws4_request\u0026X-Amz-SignedHeaders=host\u0026X-Amz-Signature=620c918f9d1b900bcefcb3c054992f1240548311ada9a4fa56b36f61816b1d64","http://127.0.0.1:9000/packages/tmp/uploads/1639751506-21670-0002-9628-c1050a94854bc6fb445cd2b2e4ee543b?uploadId=02f158fb-ac7e-4c85-8d04-ae116839c24e\u0026partNumber=37\u0026X-Amz-Expires=15300\u0026X-Amz-Date=20211209T155658Z\u0026X-Amz-Algorithm=AWS4-HMAC-SHA256\u0026X-Amz-Credential=minio%2F20211209%2Fgdk%2Fs3%2Faws4_request\u0026X-Amz-SignedHeaders=host\u0026X-Amz-Signature=c241d6ceaf79ceedf5f845504026f1b67a846e7d80091da37744b48c3c374398","http://127.0.0.1:9000/packages/tmp/uploads/1639751506-21670-0002-9628-c1050a94854bc6fb445cd2b2e4ee543b?uploadId=02f158fb-ac7e-4c85-8d04-ae116839c24e\u0026partNumber=38\u0026X-Amz-Expires=15300\u0026X-Amz-Date=20211209T155658Z\u0026X-Amz-Algorithm=AWS4-HMAC-SHA256\u0026X-Amz-Credential=minio%2F20211209%2Fgdk%2Fs3%2Faws4_request\u0026X-Amz-SignedHeaders=host\u0026X-Amz-Signature=035bf54559be55c35f4657ea05cf331617458db14f1f7ca3e8ec7bbe242edb49","http://127.0.0.1:9000/packages/tmp/uploads/1639751506-21670-0002-9628-c1050a94854bc6fb445cd2b2e4ee543b?uploadId=02f158fb-ac7e-4c85-8d04-ae116839c24e\u0026partNumber=39\u0026X-Amz-Expires=15300\u0026X-Amz-Date=20211209T155658Z\u0026X-Amz-Algorithm=AWS4-HMAC-SHA256\u0026X-Amz-Credential=minio%2F20211209%2Fgdk%2Fs3%2Faws4_request\u0026X-Amz-SignedHeaders=host\u0026X-Amz-Signature=37edea291e64ed3a26e9e3375e8e5cda4b2f891de78b1c8aef694ac50fe59f18","http://127.0.0.1:9000/packages/tmp/uploads/1639751506-21670-0002-9628-c1050a94854bc6fb445cd2b2e4ee543b?uploadId=02f158fb-ac7e-4c85-8d04-ae116839c24e\u0026partNumber=40\u0026X-Amz-Expires=15300\u0026X-Amz-Date=20211209T155658Z\u0026X-Amz-Algorithm=AWS4-HMAC-SHA256\u0026X-Amz-Credential=minio%2F20211209%2Fgdk%2Fs3%2Faws4_request\u0026X-Amz-SignedHeaders=host\u0026X-Amz-Signature=05745ef3e8599b0e1e5983dbfe30bc1b1f69da328af88f81eccd3bd465ea7d3c","http://127.0.0.1:9000/packages/tmp/uploads/1639751506-21670-0002-9628-c1050a94854bc6fb445cd2b2e4ee543b?uploadId=02f158fb-ac7e-4c85-8d04-ae116839c24e\u0026partNumber=41\u0026X-Amz-Expires=15300\u0026X-Amz-Date=20211209T155658Z\u0026X-Amz-Algorithm=AWS4-HMAC-SHA256\u0026X-Amz-Credential=minio%2F20211209%2Fgdk%2Fs3%2Faws4_request\u0026X-Amz-SignedHeaders=host\u0026X-Amz-Signature=a847ff354afe042d2fd03e6e2a9ead719d7ccea113590dba14adbecd8eb348dc","http://127.0.0.1:9000/packages/tmp/uploads/1639751506-21670-0002-9628-c1050a94854bc6fb445cd2b2e4ee543b?uploadId=02f158fb-ac7e-4c85-8d04-ae116839c24e\u0026partNumber=42\u0026X-Amz-Expires=15300\u0026X-Amz-Date=20211209T155658Z\u0026X-Amz-Algorithm=AWS4-HMAC-SHA256\u0026X-Amz-Credential=minio%2F20211209%2Fgdk%2Fs3%2Faws4_request\u0026X-Amz-SignedHeaders=host\u0026X-Amz-Signature=59f7b1b35d8dc35c371d27de32db67dddd3c521e88f336fb5da985c0d219dd43","http://127.0.0.1:9000/packages/tmp/uploads/1639751506-21670-0002-9628-c1050a94854bc6fb445cd2b2e4ee543b?uploadId=02f158fb-ac7e-4c85-8d04-ae116839c24e\u0026partNumber=43\u0026X-Amz-Expires=15300\u0026X-Amz-Date=20211209T155658Z\u0026X-Amz-Algorithm=AWS4-HMAC-SHA256\u0026X-Amz-Credential=minio%2F20211209%2Fgdk%2Fs3%2Faws4_request\u0026X-Amz-SignedHeaders=host\u0026X-Amz-Signature=88fda964b7cada361d00b94dfe26db8dd5961cb44b34dde72087200b145e6fb3","http://127.0.0.1:9000/packages/tmp/uploads/1639751506-21670-0002-9628-c1050a94854bc6fb445cd2b2e4ee543b?uploadId=02f158fb-ac7e-4c85-8d04-ae116839c24e\u0026partNumber=44\u0026X-Amz-Expires=15300\u0026X-Amz-Date=20211209T155658Z\u0026X-Amz-Algorithm=AWS4-HMAC-SHA256\u0026X-Amz-Credential=minio%2F20211209%2Fgdk%2Fs3%2Faws4_request\u0026X-Amz-SignedHeaders=host\u0026X-Amz-Signature=f498401e30ebba8bf33ae94b6e4d8ff7ee00aff96f54f3034bb09001077478aa","http://127.0.0.1:9000/packages/tmp/uploads/1639751506-21670-0002-9628-c1050a94854bc6fb445cd2b2e4ee543b?uploadId=02f158fb-ac7e-4c85-8d04-ae116839c24e\u0026partNumber=45\u0026X-Amz-Expires=15300\u0026X-Amz-Date=20211209T155658Z\u0026X-Amz-Algorithm=AWS4-HMAC-SHA256\u0026X-Amz-Credential=minio%2F20211209%2Fgdk%2Fs3%2Faws4_request\u0026X-Amz-SignedHeaders=host\u0026X-Amz-Signature=3db3023032aebaffb5db437b3decced103c421b75f0c1bdb8effaa109f0d511e","http://127.0.0.1:9000/packages/tmp/uploads/1639751506-21670-0002-9628-c1050a94854bc6fb445cd2b2e4ee543b?uploadId=02f158fb-ac7e-4c85-8d04-ae116839c24e\u0026partNumber=46\u0026X-Amz-Expires=15300\u0026X-Amz-Date=20211209T155658Z\u0026X-Amz-Algorithm=AWS4-HMAC-SHA256\u0026X-Amz-Credential=minio%2F20211209%2Fgdk%2Fs3%2Faws4_request\u0026X-Amz-SignedHeaders=host\u0026X-Amz-Signature=d675a45080c711f294d7e1938e65d7b87135d05bbcb0ee41685144ee9eef1a15","http://127.0.0.1:9000/packages/tmp/uploads/1639751506-21670-0002-9628-c1050a94854bc6fb445cd2b2e4ee543b?uploadId=02f158fb-ac7e-4c85-8d04-ae116839c24e\u0026partNumber=47\u0026X-Amz-Expires=15300\u0026X-Amz-Date=20211209T155658Z\u0026X-Amz-Algorithm=AWS4-HMAC-SHA256\u0026X-Amz-Credential=minio%2F20211209%2Fgdk%2Fs3%2Faws4_request\u0026X-Amz-SignedHeaders=host\u0026X-Amz-Signature=1f2de17d04d9861ba3bd58e3a0f0b4827ab262715a84f8d6da019c56d4489d48","http://127.0.0.1:9000/packages/tmp/uploads/1639751506-21670-0002-9628-c1050a94854bc6fb445cd2b2e4ee543b?uploadId=02f158fb-ac7e-4c85-8d04-ae116839c24e\u0026partNumber=48\u0026X-Amz-Expires=15300\u0026X-Amz-Date=20211209T155658Z\u0026X-Amz-Algorithm=AWS4-HMAC-SHA256\u0026X-Amz-Credential=minio%2F20211209%2Fgdk%2Fs3%2Faws4_request\u0026X-Amz-SignedHeaders=host\u0026X-Amz-Signature=9a55f53dad3fb665db55b471e550d94e80cffb707441e33f0a87958ac520a0d1","http://127.0.0.1:9000/packages/tmp/uploads/1639751506-21670-0002-9628-c1050a94854bc6fb445cd2b2e4ee543b?uploadId=02f158fb-ac7e-4c85-8d04-ae116839c24e\u0026partNumber=49\u0026X-Amz-Expires=15300\u0026X-Amz-Date=20211209T155658Z\u0026X-Amz-Algorithm=AWS4-HMAC-SHA256\u0026X-Amz-Credential=minio%2F20211209%2Fgdk%2Fs3%2Faws4_request\u0026X-Amz-SignedHeaders=host\u0026X-Amz-Signature=16a00a87376dc3e01db0dc4531b57177ee10be33be6b376f39796a315ff152e4","http://127.0.0.1:9000/packages/tmp/uploads/1639751506-21670-0002-9628-c1050a94854bc6fb445cd2b2e4ee543b?uploadId=02f158fb-ac7e-4c85-8d04-ae116839c24e\u0026partNumber=50\u0026X-Amz-Expires=15300\u0026X-Amz-Date=20211209T155658Z\u0026X-Amz-Algorithm=AWS4-HMAC-SHA256\u0026X-Amz-Credential=minio%2F20211209%2Fgdk%2Fs3%2Faws4_request\u0026X-Amz-SignedHeaders=host\u0026X-Amz-Signature=2e251d847c8e59c30ca079d9f11777f4f1da6c2985a65707f14017c77b5846b6","http://127.0.0.1:9000/packages/tmp/uploads/1639751506-21670-0002-9628-c1050a94854bc6fb445cd2b2e4ee543b?uploadId=02f158fb-ac7e-4c85-8d04-ae116839c24e\u0026partNumber=51\u0026X-Amz-Expires=15300\u0026X-Amz-Date=20211209T155658Z\u0026X-Amz-Algorithm=AWS4-HMAC-SHA256\u0026X-Amz-Credential=minio%2F20211209%2Fgdk%2Fs3%2Faws4_request\u0026X-Amz-SignedHeaders=host\u0026X-Amz-Signature=7ea4dd7bb05a74387b50ccba58afd45a562ad534834a7f2972a64fcd491cc3d7","http://127.0.0.1:9000/packages/tmp/uploads/1639751506-21670-0002-9628-c1050a94854bc6fb445cd2b2e4ee543b?uploadId=02f158fb-ac7e-4c85-8d04-ae116839c24e\u0026partNumber=52\u0026X-Amz-Expires=15300\u0026X-Amz-Date=20211209T155658Z\u0026X-Amz-Algorithm=AWS4-HMAC-SHA256\u0026X-Amz-Credential=minio%2F20211209%2Fgdk%2Fs3%2Faws4_request\u0026X-Amz-SignedHeaders=host\u0026X-Amz-Signature=7bfd82edda1e6722ce71d21a669eb075ea98e914936915f6a425cd6468e427a9","http://127.0.0.1:9000/packages/tmp/uploads/1639751506-21670-0002-9628-c1050a94854bc6fb445cd2b2e4ee543b?uploadId=02f158fb-ac7e-4c85-8d04-ae116839c24e\u0026partNumber=53\u0026X-Amz-Expires=15299\u0026X-Amz-Date=20211209T155659Z\u0026X-Amz-Algorithm=AWS4-HMAC-SHA256\u0026X-Amz-Credential=minio%2F20211209%2Fgdk%2Fs3%2Faws4_request\u0026X-Amz-SignedHeaders=host\u0026X-Amz-Signature=b1b7f1c0d121ce0efee03e404606fdf5d01c74e79d29431d0f72d16e18d471cb","http://127.0.0.1:9000/packages/tmp/uploads/1639751506-21670-0002-9628-c1050a94854bc6fb445cd2b2e4ee543b?uploadId=02f158fb-ac7e-4c85-8d04-ae116839c24e\u0026partNumber=54\u0026X-Amz-Expires=15299\u0026X-Amz-Date=20211209T155659Z\u0026X-Amz-Algorithm=AWS4-HMAC-SHA256\u0026X-Amz-Credential=minio%2F20211209%2Fgdk%2Fs3%2Faws4_request\u0026X-Amz-SignedHeaders=host\u0026X-Amz-Signature=f1667168c1f51ff05f9f7f090d0a731f22516a4e8da7921d1e4d2037af8f9bcf","http://127.0.0.1:9000/packages/tmp/uploads/1639751506-21670-0002-9628-c1050a94854bc6fb445cd2b2e4ee543b?uploadId=02f158fb-ac7e-4c85-8d04-ae116839c24e\u0026partNumber=55\u0026X-Amz-Expires=15299\u0026X-Amz-Date=20211209T155659Z\u0026X-Amz-Algorithm=AWS4-HMAC-SHA256\u0026X-Amz-Credential=minio%2F20211209%2Fgdk%2Fs3%2Faws4_request\u0026X-Amz-SignedHeaders=host\u0026X-Amz-Signature=c6705e84dc9e1e97a133bae1654dc8420da26731e51cc9b4fd568903fbaf9338","http://127.0.0.1:9000/packages/tmp/uploads/1639751506-21670-0002-9628-c1050a94854bc6fb445cd2b2e4ee543b?uploadId=02f158fb-ac7e-4c85-8d04-ae116839c24e\u0026partNumber=56\u0026X-Amz-Expires=15299\u0026X-Amz-Date=20211209T155659Z\u0026X-Amz-Algorithm=AWS4-HMAC-SHA256\u0026X-Amz-Credential=minio%2F20211209%2Fgdk%2Fs3%2Faws4_request\u0026X-Amz-SignedHeaders=host\u0026X-Amz-Signature=91cdf4d3b3bcc04ba9ecfc76e0c48fa6578a7b97fa3044aa2ab33baec021bc42","http://127.0.0.1:9000/packages/tmp/uploads/1639751506-21670-0002-9628-c1050a94854bc6fb445cd2b2e4ee543b?uploadId=02f158fb-ac7e-4c85-8d04-ae116839c24e\u0026partNumber=57\u0026X-Amz-Expires=15299\u0026X-Amz-Date=20211209T155659Z\u0026X-Amz-Algorithm=AWS4-HMAC-SHA256\u0026X-Amz-Credential=minio%2F20211209%2Fgdk%2Fs3%2Faws4_request\u0026X-Amz-SignedHeaders=host\u0026X-Amz-Signature=e46b3f5daf08990c884a44f7c7646910a71e8b96a2db74091fd98c2af0c493d7","http://127.0.0.1:9000/packages/tmp/uploads/1639751506-21670-0002-9628-c1050a94854bc6fb445cd2b2e4ee543b?uploadId=02f158fb-ac7e-4c85-8d04-ae116839c24e\u0026partNumber=58\u0026X-Amz-Expires=15299\u0026X-Amz-Date=20211209T155659Z\u0026X-Amz-Algorithm=AWS4-HMAC-SHA256\u0026X-Amz-Credential=minio%2F20211209%2Fgdk%2Fs3%2Faws4_request\u0026X-Amz-SignedHeaders=host\u0026X-Amz-Signature=4441def7fc25f18ddc29e178d8d4dd7e3b67b9256b5e7a54b05a1e7474a1ffa7","http://127.0.0.1:9000/packages/tmp/uploads/1639751506-21670-0002-9628-c1050a94854bc6fb445cd2b2e4ee543b?uploadId=02f158fb-ac7e-4c85-8d04-ae116839c24e\u0026partNumber=59\u0026X-Amz-Expires=15299\u0026X-Amz-Date=20211209T155659Z\u0026X-Amz-Algorithm=AWS4-HMAC-SHA256\u0026X-Amz-Credential=minio%2F20211209%2Fgdk%2Fs3%2Faws4_request\u0026X-Amz-SignedHeaders=host\u0026X-Amz-Signature=c554ea418b58ea3b1e9574cdeec0bd07b2076098e246117b6fcae62ff6c28c9b","http://127.0.0.1:9000/packages/tmp/uploads/1639751506-21670-0002-9628-c1050a94854bc6fb445cd2b2e4ee543b?uploadId=02f158fb-ac7e-4c85-8d04-ae116839c24e\u0026partNumber=60\u0026X-Amz-Expires=15299\u0026X-Amz-Date=20211209T155659Z\u0026X-Amz-Algorithm=AWS4-HMAC-SHA256\u0026X-Amz-Credential=minio%2F20211209%2Fgdk%2Fs3%2Faws4_request\u0026X-Amz-SignedHeaders=host\u0026X-Amz-Signature=17eb2e1bdbb57b83da9e22c58a06e2426cc7317ef49357d47d4fbc9d8b008540","http://127.0.0.1:9000/packages/tmp/uploads/1639751506-21670-0002-9628-c1050a94854bc6fb445cd2b2e4ee543b?uploadId=02f158fb-ac7e-4c85-8d04-ae116839c24e\u0026partNumber=61\u0026X-Amz-Expires=15299\u0026X-Amz-Date=20211209T155659Z\u0026X-Amz-Algorithm=AWS4-HMAC-SHA256\u0026X-Amz-Credential=minio%2F20211209%2Fgdk%2Fs3%2Faws4_request\u0026X-Amz-SignedHeaders=host\u0026X-Amz-Signature=1d22c9182d688d0bb224017a258225f08b0031c2d77bcf6f7c220632373c6ca2","http://127.0.0.1:9000/packages/tmp/uploads/1639751506-21670-0002-9628-c1050a94854bc6fb445cd2b2e4ee543b?uploadId=02f158fb-ac7e-4c85-8d04-ae116839c24e\u0026partNumber=62\u0026X-Amz-Expires=15299\u0026X-Amz-Date=20211209T155659Z\u0026X-Amz-Algorithm=AWS4-HMAC-SHA256\u0026X-Amz-Credential=minio%2F20211209%2Fgdk%2Fs3%2Faws4_request\u0026X-Amz-SignedHeaders=host\u0026X-Amz-Signature=262a12efcb05bf24a1ee4b6560516777480f08ec212f7c897d735d5214c21f8d","http://127.0.0.1:9000/packages/tmp/uploads/1639751506-21670-0002-9628-c1050a94854bc6fb445cd2b2e4ee543b?uploadId=02f158fb-ac7e-4c85-8d04-ae116839c24e\u0026partNumber=63\u0026X-Amz-Expires=15299\u0026X-Amz-Date=20211209T155659Z\u0026X-Amz-Algorithm=AWS4-HMAC-SHA256\u0026X-Amz-Credential=minio%2F20211209%2Fgdk%2Fs3%2Faws4_request\u0026X-Amz-SignedHeaders=host\u0026X-Amz-Signature=fac2715170225d37fbe34f6baf8416eaebfefabd1cb94119f3f4ce5da26a8948","http://127.0.0.1:9000/packages/tmp/uploads/1639751506-21670-0002-9628-c1050a94854bc6fb445cd2b2e4ee543b?uploadId=02f158fb-ac7e-4c85-8d04-ae116839c24e\u0026partNumber=64\u0026X-Amz-Expires=15299\u0026X-Amz-Date=20211209T155659Z\u0026X-Amz-Algorithm=AWS4-HMAC-SHA256\u0026X-Amz-Credential=minio%2F20211209%2Fgdk%2Fs3%2Faws4_request\u0026X-Amz-SignedHeaders=host\u0026X-Amz-Signature=789711efb54be83c44368e7b9bf61080ad51d38210a4e7d958de8368c20c0fc4","http://127.0.0.1:9000/packages/tmp/uploads/1639751506-21670-0002-9628-c1050a94854bc6fb445cd2b2e4ee543b?uploadId=02f158fb-ac7e-4c85-8d04-ae116839c24e\u0026partNumber=65\u0026X-Amz-Expires=15299\u0026X-Amz-Date=20211209T155659Z\u0026X-Amz-Algorithm=AWS4-HMAC-SHA256\u0026X-Amz-Credential=minio%2F20211209%2Fgdk%2Fs3%2Faws4_request\u0026X-Amz-SignedHeaders=host\u0026X-Amz-Signature=c78b7f59477d7ce5b096329c10e956d46de858ced212cb12c8951b2f23161fd6","http://127.0.0.1:9000/packages/tmp/uploads/1639751506-21670-0002-9628-c1050a94854bc6fb445cd2b2e4ee543b?uploadId=02f158fb-ac7e-4c85-8d04-ae116839c24e\u0026partNumber=66\u0026X-Amz-Expires=15299\u0026X-Amz-Date=20211209T155659Z\u0026X-Amz-Algorithm=AWS4-HMAC-SHA256\u0026X-Amz-Credential=minio%2F20211209%2Fgdk%2Fs3%2Faws4_request\u0026X-Amz-SignedHeaders=host\u0026X-Amz-Signature=e8bc5bc1ffb576b3697d7201b10109a91f7f06349d3b882d9c7163ff56fd71ba","http://127.0.0.1:9000/packages/tmp/uploads/1639751506-21670-0002-9628-c1050a94854bc6fb445cd2b2e4ee543b?uploadId=02f158fb-ac7e-4c85-8d04-ae116839c24e\u0026partNumber=67\u0026X-Amz-Expires=15299\u0026X-Amz-Date=20211209T155659Z\u0026X-Amz-Algorithm=AWS4-HMAC-SHA256\u0026X-Amz-Credential=minio%2F20211209%2Fgdk%2Fs3%2Faws4_request\u0026X-Amz-SignedHeaders=host\u0026X-Amz-Signature=c9d76549752f02ca2b1d0271704ce92ce471ff74b0e3bc4d18830b52c20c1fcf","http://127.0.0.1:9000/packages/tmp/uploads/1639751506-21670-0002-9628-c1050a94854bc6fb445cd2b2e4ee543b?uploadId=02f158fb-ac7e-4c85-8d04-ae116839c24e\u0026partNumber=68\u0026X-Amz-Expires=15299\u0026X-Amz-Date=20211209T155659Z\u0026X-Amz-Algorithm=AWS4-HMAC-SHA256\u0026X-Amz-Credential=minio%2F20211209%2Fgdk%2Fs3%2Faws4_request\u0026X-Amz-SignedHeaders=host\u0026X-Amz-Signature=0365396dde42f0b576b9ce17ec67965eccaed1b103ab36402c37d8836b939a92","http://127.0.0.1:9000/packages/tmp/uploads/1639751506-21670-0002-9628-c1050a94854bc6fb445cd2b2e4ee543b?uploadId=02f158fb-ac7e-4c85-8d04-ae116839c24e\u0026partNumber=69\u0026X-Amz-Expires=15299\u0026X-Amz-Date=20211209T155659Z\u0026X-Amz-Algorithm=AWS4-HMAC-SHA256\u0026X-Amz-Credential=minio%2F20211209%2Fgdk%2Fs3%2Faws4_request\u0026X-Amz-SignedHeaders=host\u0026X-Amz-Signature=5dc19b5d4e4419c6c06894311081aef464db93056cdf6af023bc71235e62bf49","http://127.0.0.1:9000/packages/tmp/uploads/1639751506-21670-0002-9628-c1050a94854bc6fb445cd2b2e4ee543b?uploadId=02f158fb-ac7e-4c85-8d04-ae116839c24e\u0026partNumber=70\u0026X-Amz-Expires=15299\u0026X-Amz-Date=20211209T155659Z\u0026X-Amz-Algorithm=AWS4-HMAC-SHA256\u0026X-Amz-Credential=minio%2F20211209%2Fgdk%2Fs3%2Faws4_request\u0026X-Amz-SignedHeaders=host\u0026X-Amz-Signature=513b15b150c7bffa9df1102dc0ad32c97f4f7a9b65fb53b0c56c9ecdf963180b","http://127.0.0.1:9000/packages/tmp/uploads/1639751506-21670-0002-9628-c1050a94854bc6fb445cd2b2e4ee543b?uploadId=02f158fb-ac7e-4c85-8d04-ae116839c24e\u0026partNumber=71\u0026X-Amz-Expires=15299\u0026X-Amz-Date=20211209T155659Z\u0026X-Amz-Algorithm=AWS4-HMAC-SHA256\u0026X-Amz-Credential=minio%2F20211209%2Fgdk%2Fs3%2Faws4_request\u0026X-Amz-SignedHeaders=host\u0026X-Amz-Signature=c6c8e2885cc5a3d8cbc811b7ed971200ec4decc183f2ddbd57b8a5fdc61c4999","http://127.0.0.1:9000/packages/tmp/uploads/1639751506-21670-0002-9628-c1050a94854bc6fb445cd2b2e4ee543b?uploadId=02f158fb-ac7e-4c85-8d04-ae116839c24e\u0026partNumber=72\u0026X-Amz-Expires=15299\u0026X-Amz-Date=20211209T155659Z\u0026X-Amz-Algorithm=AWS4-HMAC-SHA256\u0026X-Amz-Credential=minio%2F20211209%2Fgdk%2Fs3%2Faws4_request\u0026X-Amz-SignedHeaders=host\u0026X-Amz-Signature=093da9aa9c22999acde538b57fd7821dbc7a28ac3d755ac07f52488a43fe2bfa","http://127.0.0.1:9000/packages/tmp/uploads/1639751506-21670-0002-9628-c1050a94854bc6fb445cd2b2e4ee543b?uploadId=02f158fb-ac7e-4c85-8d04-ae116839c24e\u0026partNumber=73\u0026X-Amz-Expires=15299\u0026X-Amz-Date=20211209T155659Z\u0026X-Amz-Algorithm=AWS4-HMAC-SHA256\u0026X-Amz-Credential=minio%2F20211209%2Fgdk%2Fs3%2Faws4_request\u0026X-Amz-SignedHeaders=host\u0026X-Amz-Signature=00e1e70bbed8b782efac4716dad03c57f7940340b6ee38d49aea2e503d6bd976","http://127.0.0.1:9000/packages/tmp/uploads/1639751506-21670-0002-9628-c1050a94854bc6fb445cd2b2e4ee543b?uploadId=02f158fb-ac7e-4c85-8d04-ae116839c24e\u0026partNumber=74\u0026X-Amz-Expires=15299\u0026X-Amz-Date=20211209T155659Z\u0026X-Amz-Algorithm=AWS4-HMAC-SHA256\u0026X-Amz-Credential=minio%2F20211209%2Fgdk%2Fs3%2Faws4_request\u0026X-Amz-SignedHeaders=host\u0026X-Amz-Signature=213de16b24804571a92f0cf84c17cc1f15ff6b8a0ebc3e0d9af0d4f2bf8dfa36","http://127.0.0.1:9000/packages/tmp/uploads/1639751506-21670-0002-9628-c1050a94854bc6fb445cd2b2e4ee543b?uploadId=02f158fb-ac7e-4c85-8d04-ae116839c24e\u0026partNumber=75\u0026X-Amz-Expires=15299\u0026X-Amz-Date=20211209T155659Z\u0026X-Amz-Algorithm=AWS4-HMAC-SHA256\u0026X-Amz-Credential=minio%2F20211209%2Fgdk%2Fs3%2Faws4_request\u0026X-Amz-SignedHeaders=host\u0026X-Amz-Signature=6cf528593d6e48b21056f16f167de028132c25add675659b505b6f06f74e8622","http://127.0.0.1:9000/packages/tmp/uploads/1639751506-21670-0002-9628-c1050a94854bc6fb445cd2b2e4ee543b?uploadId=02f158fb-ac7e-4c85-8d04-ae116839c24e\u0026partNumber=76\u0026X-Amz-Expires=15299\u0026X-Amz-Date=20211209T155659Z\u0026X-Amz-Algorithm=AWS4-HMAC-SHA256\u0026X-Amz-Credential=minio%2F20211209%2Fgdk%2Fs3%2Faws4_request\u0026X-Amz-SignedHeaders=host\u0026X-Amz-Signature=9df0d040daafda34013ff8e591fd374fbebe5aa35a3448e874544b8eae22e680","http://127.0.0.1:9000/packages/tmp/uploads/1639751506-21670-0002-9628-c1050a94854bc6fb445cd2b2e4ee543b?uploadId=02f158fb-ac7e-4c85-8d04-ae116839c24e\u0026partNumber=77\u0026X-Amz-Expires=15299\u0026X-Amz-Date=20211209T155659Z\u0026X-Amz-Algorithm=AWS4-HMAC-SHA256\u0026X-Amz-Credential=minio%2F20211209%2Fgdk%2Fs3%2Faws4_request\u0026X-Amz-SignedHeaders=host\u0026X-Amz-Signature=e24b6c3fe5249a6a21e0f772d1863453736498dcb9d771efca85501a0f2b38b0","http://127.0.0.1:9000/packages/tmp/uploads/1639751506-21670-0002-9628-c1050a94854bc6fb445cd2b2e4ee543b?uploadId=02f158fb-ac7e-4c85-8d04-ae116839c24e\u0026partNumber=78\u0026X-Amz-Expires=15299\u0026X-Amz-Date=20211209T155659Z\u0026X-Amz-Algorithm=AWS4-HMAC-SHA256\u0026X-Amz-Credential=minio%2F20211209%2Fgdk%2Fs3%2Faws4_request\u0026X-Amz-SignedHeaders=host\u0026X-Amz-Signature=28d6ee77d6fe987506baa7ada7bae7fbd8347492bf6fca31d00e92ae5d7edac5","http://127.0.0.1:9000/packages/tmp/uploads/1639751506-21670-0002-9628-c1050a94854bc6fb445cd2b2e4ee543b?uploadId=02f158fb-ac7e-4c85-8d04-ae116839c24e\u0026partNumber=79\u0026X-Amz-Expires=15299\u0026X-Amz-Date=20211209T155659Z\u0026X-Amz-Algorithm=AWS4-HMAC-SHA256\u0026X-Amz-Credential=minio%2F20211209%2Fgdk%2Fs3%2Faws4_request\u0026X-Amz-SignedHeaders=host\u0026X-Amz-Signature=1f248519544fd700be1d0d53ab995cdb2149e5737f9b907fdfcecc029ec6c77b","http://127.0.0.1:9000/packages/tmp/uploads/1639751506-21670-0002-9628-c1050a94854bc6fb445cd2b2e4ee543b?uploadId=02f158fb-ac7e-4c85-8d04-ae116839c24e\u0026partNumber=80\u0026X-Amz-Expires=15299\u0026X-Amz-Date=20211209T155659Z\u0026X-Amz-Algorithm=AWS4-HMAC-SHA256\u0026X-Amz-Credential=minio%2F20211209%2Fgdk%2Fs3%2Faws4_request\u0026X-Amz-SignedHeaders=host\u0026X-Amz-Signature=d232eebd7d41373a9d1b3f60bbde1629c82c0eada818e9d953f017265e0d96f6","http://127.0.0.1:9000/packages/tmp/uploads/1639751506-21670-0002-9628-c1050a94854bc6fb445cd2b2e4ee543b?uploadId=02f158fb-ac7e-4c85-8d04-ae116839c24e\u0026partNumber=81\u0026X-Amz-Expires=15299\u0026X-Amz-Date=20211209T155659Z\u0026X-Amz-Algorithm=AWS4-HMAC-SHA256\u0026X-Amz-Credential=minio%2F20211209%2Fgdk%2Fs3%2Faws4_request\u0026X-Amz-SignedHeaders=host\u0026X-Amz-Signature=2ec2ef71e58ae78b02d8aecf57356da224c6ec3ff0a4e4673c9c9361b4bcbb57","http://127.0.0.1:9000/packages/tmp/uploads/1639751506-21670-0002-9628-c1050a94854bc6fb445cd2b2e4ee543b?uploadId=02f158fb-ac7e-4c85-8d04-ae116839c24e\u0026partNumber=82\u0026X-Amz-Expires=15299\u0026X-Amz-Date=20211209T155659Z\u0026X-Amz-Algorithm=AWS4-HMAC-SHA256\u0026X-Amz-Credential=minio%2F20211209%2Fgdk%2Fs3%2Faws4_request\u0026X-Amz-SignedHeaders=host\u0026X-Amz-Signature=33d500daae468cecea8b5f5c898673372c16f3c97797d6f48f1d5549d0882647","http://127.0.0.1:9000/packages/tmp/uploads/1639751506-21670-0002-9628-c1050a94854bc6fb445cd2b2e4ee543b?uploadId=02f158fb-ac7e-4c85-8d04-ae116839c24e\u0026partNumber=83\u0026X-Amz-Expires=15299\u0026X-Amz-Date=20211209T155659Z\u0026X-Amz-Algorithm=AWS4-HMAC-SHA256\u0026X-Amz-Credential=minio%2F20211209%2Fgdk%2Fs3%2Faws4_request\u0026X-Amz-SignedHeaders=host\u0026X-Amz-Signature=0037ad82aa1e5433c0e1dd04d5b2b510f3b620059c14f3d0a1f92f1f82f7158f","http://127.0.0.1:9000/packages/tmp/uploads/1639751506-21670-0002-9628-c1050a94854bc6fb445cd2b2e4ee543b?uploadId=02f158fb-ac7e-4c85-8d04-ae116839c24e\u0026partNumber=84\u0026X-Amz-Expires=15299\u0026X-Amz-Date=20211209T155659Z\u0026X-Amz-Algorithm=AWS4-HMAC-SHA256\u0026X-Amz-Credential=minio%2F20211209%2Fgdk%2Fs3%2Faws4_request\u0026X-Amz-SignedHeaders=host\u0026X-Amz-Signature=e0caa2eb4e75202e9478321254343730532f5f308f60bba03a35a2fceb73594b","http://127.0.0.1:9000/packages/tmp/uploads/1639751506-21670-0002-9628-c1050a94854bc6fb445cd2b2e4ee543b?uploadId=02f158fb-ac7e-4c85-8d04-ae116839c24e\u0026partNumber=85\u0026X-Amz-Expires=15299\u0026X-Amz-Date=20211209T155659Z\u0026X-Amz-Algorithm=AWS4-HMAC-SHA256\u0026X-Amz-Credential=minio%2F20211209%2Fgdk%2Fs3%2Faws4_request\u0026X-Amz-SignedHeaders=host\u0026X-Amz-Signature=297f2a118b92b30a6f3a56d5f235508ec3f11a9adb0a27fbe7a234edb6ac9555","http://127.0.0.1:9000/packages/tmp/uploads/1639751506-21670-0002-9628-c1050a94854bc6fb445cd2b2e4ee543b?uploadId=02f158fb-ac7e-4c85-8d04-ae116839c24e\u0026partNumber=86\u0026X-Amz-Expires=15299\u0026X-Amz-Date=20211209T155659Z\u0026X-Amz-Algorithm=AWS4-HMAC-SHA256\u0026X-Amz-Credential=minio%2F20211209%2Fgdk%2Fs3%2Faws4_request\u0026X-Amz-SignedHeaders=host\u0026X-Amz-Signature=70512e2527c2990b5ec036a34334880482d23383f425a63c7188d284d1224129","http://127.0.0.1:9000/packages/tmp/uploads/1639751506-21670-0002-9628-c1050a94854bc6fb445cd2b2e4ee543b?uploadId=02f158fb-ac7e-4c85-8d04-ae116839c24e\u0026partNumber=87\u0026X-Amz-Expires=15299\u0026X-Amz-Date=20211209T155659Z\u0026X-Amz-Algorithm=AWS4-HMAC-SHA256\u0026X-Amz-Credential=minio%2F20211209%2Fgdk%2Fs3%2Faws4_request\u0026X-Amz-SignedHeaders=host\u0026X-Amz-Signature=019b61ae21e555a5bb7f816d39c8b3ce6b3081f31f63ca8aff0f2b8be52a2e4f","http://127.0.0.1:9000/packages/tmp/uploads/1639751506-21670-0002-9628-c1050a94854bc6fb445cd2b2e4ee543b?uploadId=02f158fb-ac7e-4c85-8d04-ae116839c24e\u0026partNumber=88\u0026X-Amz-Expires=15299\u0026X-Amz-Date=20211209T155659Z\u0026X-Amz-Algorithm=AWS4-HMAC-SHA256\u0026X-Amz-Credential=minio%2F20211209%2Fgdk%2Fs3%2Faws4_request\u0026X-Amz-SignedHeaders=host\u0026X-Amz-Signature=0177d1122c8d5425aa3f7daa209630a4f6fd7599969473017932ea272421d74f","http://127.0.0.1:9000/packages/tmp/uploads/1639751506-21670-0002-9628-c1050a94854bc6fb445cd2b2e4ee543b?uploadId=02f158fb-ac7e-4c85-8d04-ae116839c24e\u0026partNumber=89\u0026X-Amz-Expires=15299\u0026X-Amz-Date=20211209T155659Z\u0026X-Amz-Algorithm=AWS4-HMAC-SHA256\u0026X-Amz-Credential=minio%2F20211209%2Fgdk%2Fs3%2Faws4_request\u0026X-Amz-SignedHeaders=host\u0026X-Amz-Signature=80ee0bc11f7e9d30364e72103da6c81bf5b9604b78834ab34dff1ba39f860f45","http://127.0.0.1:9000/packages/tmp/uploads/1639751506-21670-0002-9628-c1050a94854bc6fb445cd2b2e4ee543b?uploadId=02f158fb-ac7e-4c85-8d04-ae116839c24e\u0026partNumber=90\u0026X-Amz-Expires=15299\u0026X-Amz-Date=20211209T155659Z\u0026X-Amz-Algorithm=AWS4-HMAC-SHA256\u0026X-Amz-Credential=minio%2F20211209%2Fgdk%2Fs3%2Faws4_request\u0026X-Amz-SignedHeaders=host\u0026X-Amz-Signature=739219394704691ddd0caa23b5d6f00dc4d4444300def34cf210391b38d401b5","http://127.0.0.1:9000/packages/tmp/uploads/1639751506-21670-0002-9628-c1050a94854bc6fb445cd2b2e4ee543b?uploadId=02f158fb-ac7e-4c85-8d04-ae116839c24e\u0026partNumber=91\u0026X-Amz-Expires=15299\u0026X-Amz-Date=20211209T155659Z\u0026X-Amz-Algorithm=AWS4-HMAC-SHA256\u0026X-Amz-Credential=minio%2F20211209%2Fgdk%2Fs3%2Faws4_request\u0026X-Amz-SignedHeaders=host\u0026X-Amz-Signature=7388003796978fedf90761750ac6c2488b0a8d2d5ded6b2cdb047927777ea370","http://127.0.0.1:9000/packages/tmp/uploads/1639751506-21670-0002-9628-c1050a94854bc6fb445cd2b2e4ee543b?uploadId=02f158fb-ac7e-4c85-8d04-ae116839c24e\u0026partNumber=92\u0026X-Amz-Expires=15299\u0026X-Amz-Date=20211209T155659Z\u0026X-Amz-Algorithm=AWS4-HMAC-SHA256\u0026X-Amz-Credential=minio%2F20211209%2Fgdk%2Fs3%2Faws4_request\u0026X-Amz-SignedHeaders=host\u0026X-Amz-Signature=378a9de6e7077b2d2e935923983f3ec66f3924315f29ca9c412a250947b68827","http://127.0.0.1:9000/packages/tmp/uploads/1639751506-21670-0002-9628-c1050a94854bc6fb445cd2b2e4ee543b?uploadId=02f158fb-ac7e-4c85-8d04-ae116839c24e\u0026partNumber=93\u0026X-Amz-Expires=15299\u0026X-Amz-Date=20211209T155659Z\u0026X-Amz-Algorithm=AWS4-HMAC-SHA256\u0026X-Amz-Credential=minio%2F20211209%2Fgdk%2Fs3%2Faws4_request\u0026X-Amz-SignedHeaders=host\u0026X-Amz-Signature=1a670d65dbade93a82b8a77f5db52fc7b2f5bc81957363a13ac6fcf2d34c639d","http://127.0.0.1:9000/packages/tmp/uploads/1639751506-21670-0002-9628-c1050a94854bc6fb445cd2b2e4ee543b?uploadId=02f158fb-ac7e-4c85-8d04-ae116839c24e\u0026partNumber=94\u0026X-Amz-Expires=15299\u0026X-Amz-Date=20211209T155659Z\u0026X-Amz-Algorithm=AWS4-HMAC-SHA256\u0026X-Amz-Credential=minio%2F20211209%2Fgdk%2Fs3%2Faws4_request\u0026X-Amz-SignedHeaders=host\u0026X-Amz-Signature=96083f361e291743965c28020ff03e55fcdfcb44b504c3bb6192a40c0fa5e158","http://127.0.0.1:9000/packages/tmp/uploads/1639751506-21670-0002-9628-c1050a94854bc6fb445cd2b2e4ee543b?uploadId=02f158fb-ac7e-4c85-8d04-ae116839c24e\u0026partNumber=95\u0026X-Amz-Expires=15299\u0026X-Amz-Date=20211209T155659Z\u0026X-Amz-Algorithm=AWS4-HMAC-SHA256\u0026X-Amz-Credential=minio%2F20211209%2Fgdk%2Fs3%2Faws4_request\u0026X-Amz-SignedHeaders=host\u0026X-Amz-Signature=e85b8aebd6e417d3460273b4d6b35ca92d3daf5101b09dc1aa6dc21a475ade2d","http://127.0.0.1:9000/packages/tmp/uploads/1639751506-21670-0002-9628-c1050a94854bc6fb445cd2b2e4ee543b?uploadId=02f158fb-ac7e-4c85-8d04-ae116839c24e\u0026partNumber=96\u0026X-Amz-Expires=15299\u0026X-Amz-Date=20211209T155659Z\u0026X-Amz-Algorithm=AWS4-HMAC-SHA256\u0026X-Amz-Credential=minio%2F20211209%2Fgdk%2Fs3%2Faws4_request\u0026X-Amz-SignedHeaders=host\u0026X-Amz-Signature=2bffa014e2e1f95a0cc00eecd2754a198152a9de7ab113a8e849b4d718c686e3","http://127.0.0.1:9000/packages/tmp/uploads/1639751506-21670-0002-9628-c1050a94854bc6fb445cd2b2e4ee543b?uploadId=02f158fb-ac7e-4c85-8d04-ae116839c24e\u0026partNumber=97\u0026X-Amz-Expires=15299\u0026X-Amz-Date=20211209T155659Z\u0026X-Amz-Algorithm=AWS4-HMAC-SHA256\u0026X-Amz-Credential=minio%2F20211209%2Fgdk%2Fs3%2Faws4_request\u0026X-Amz-SignedHeaders=host\u0026X-Amz-Signature=8671891d870ba75f857c005a541471aed5e70943dff4a447522c9db635f07750","http://127.0.0.1:9000/packages/tmp/uploads/1639751506-21670-0002-9628-c1050a94854bc6fb445cd2b2e4ee543b?uploadId=02f158fb-ac7e-4c85-8d04-ae116839c24e\u0026partNumber=98\u0026X-Amz-Expires=15299\u0026X-Amz-Date=20211209T155659Z\u0026X-Amz-Algorithm=AWS4-HMAC-SHA256\u0026X-Amz-Credential=minio%2F20211209%2Fgdk%2Fs3%2Faws4_request\u0026X-Amz-SignedHeaders=host\u0026X-Amz-Signature=90b8098fd57dd0d024b8bb6a0aaa71d6cb7dfcaab639b13459188a44c208ee7f","http://127.0.0.1:9000/packages/tmp/uploads/1639751506-21670-0002-9628-c1050a94854bc6fb445cd2b2e4ee543b?uploadId=02f158fb-ac7e-4c85-8d04-ae116839c24e\u0026partNumber=99\u0026X-Amz-Expires=15299\u0026X-Amz-Date=20211209T155659Z\u0026X-Amz-Algorithm=AWS4-HMAC-SHA256\u0026X-Amz-Credential=minio%2F20211209%2Fgdk%2Fs3%2Faws4_request\u0026X-Amz-SignedHeaders=host\u0026X-Amz-Signature=1015443c298666d81acbd167dddc64f2e6138fd20659640deb0d337bc3fb4f70","http://127.0.0.1:9000/packages/tmp/uploads/1639751506-21670-0002-9628-c1050a94854bc6fb445cd2b2e4ee543b?uploadId=02f158fb-ac7e-4c85-8d04-ae116839c24e\u0026partNumber=100\u0026X-Amz-Expires=15299\u0026X-Amz-Date=20211209T155659Z\u0026X-Amz-Algorithm=AWS4-HMAC-SHA256\u0026X-Amz-Credential=minio%2F20211209%2Fgdk%2Fs3%2Faws4_request\u0026X-Amz-SignedHeaders=host\u0026X-Amz-Signature=4f472715c882e434adb7306adb64be41ff91190ff6b5ac4f253011aec25ed47c"],"CompleteURL":"http://127.0.0.1:9000/packages/tmp/uploads/1639751506-21670-0002-9628-c1050a94854bc6fb445cd2b2e4ee543b?uploadId=02f158fb-ac7e-4c85-8d04-ae116839c24e\u0026X-Amz-Expires=15299\u0026X-Amz-Date=20211209T155659Z\u0026X-Amz-Algorithm=AWS4-HMAC-SHA256\u0026X-Amz-Credential=minio%2F20211209%2Fgdk%2Fs3%2Faws4_request\u0026X-Amz-SignedHeaders=content-type%3Bhost\u0026X-Amz-Signature=041c47854e5368b8e5b0f492504d6036d31b677d2d61a849ace46c3a0c5be7fe","AbortURL":"http://127.0.0.1:9000/packages/tmp/uploads/1639751506-21670-0002-9628-c1050a94854bc6fb445cd2b2e4ee543b?uploadId=02f158fb-ac7e-4c85-8d04-ae116839c24e\u0026X-Amz-Expires=15299\u0026X-Amz-Date=20211209T155659Z\u0026X-Amz-Algorithm=AWS4-HMAC-SHA256\u0026X-Amz-Credential=minio%2F20211209%2Fgdk%2Fs3%2Faws4_request\u0026X-Amz-SignedHeaders=host\u0026X-Amz-Signature=424c2be0996328b45d93482c294cab6d8a8c8262657833cdbc6c79b74b68712f"},"CustomPutHeaders":true,"PutHeaders":{},"UseWorkhorseClient":false,"RemoteTempObjectID":"tmp/uploads/1639751506-21670-0002-9628-c1050a94854bc6fb445cd2b2e4ee543b","ObjectStorage":{"Provider":"AWS","S3Config":{"Bucket":"packages","Region":"gdk","Endpoint":"http://127.0.0.1:9000","PathStyle":true,"UseIamProfile":false}},"ID":"1639751506-21670-0002-9628-c1050a94854bc6fb445cd2b2e4ee543b"},"MaximumSize":524288000}
   ```
1. (WH) Finalize request:
   ```
   PUT /api/v4/projects/509/packages/nuget/ HTTP/1.1
   Host: gdk.test:8000
   Transfer-Encoding: chunked
   Accept-Encoding: gzip, deflate
   Authorization: Basic <encoded PAT>
   Content-Type: multipart/form-data; boundary=5c716544b0246dca90374012c8cced39cca254e061f5a8c3b2ede83dff6e
   Gitlab-Workhorse-Multipart-Fields: eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJyZXdyaXR0ZW5fZmllbGRzIjp7InBhY2thZ2UiOiIifSwiaXNzIjoiZ2l0bGFiLXdvcmtob3JzZSJ9.oATwLhyIRWL8Y32pMqhG1lodvbTejJMzGKutA-q3dm8
   User-Agent: NuGet xplat/5.8.0 (Darwin 20.6.0 Darwin Kernel Version 20.6.0: Mon Aug 30 06:12:21 PDT 2021; root:xnu-7195.141.6~3/RELEASE_X86_64)
   X-Nuget-Client-Version: 5.8.0
   X-Nuget-Session-Id: fa8ecb4f-8fb1-4360-b1d3-61f05eb209e0
   
   d48
   --5c716544b0246dca90374012c8cced39cca254e061f5a8c3b2ede83dff6e
   Content-Disposition: form-data; name="package.remote_id"
   
   1639751506-21670-0002-9628-c1050a94854bc6fb445cd2b2e4ee543b
   --5c716544b0246dca90374012c8cced39cca254e061f5a8c3b2ede83dff6e
   Content-Disposition: form-data; name="package.upload_duration"
   
   0.011574257
   --5c716544b0246dca90374012c8cced39cca254e061f5a8c3b2ede83dff6e
   Content-Disposition: form-data; name="package.md5"
   
   39293ebd923b171ec8484f891d24029b
   --5c716544b0246dca90374012c8cced39cca254e061f5a8c3b2ede83dff6e
   Content-Disposition: form-data; name="package.sha512"
   
   2d4466f4c82e7b36309b43a404bb67c013cf48f823f4665ecdb4546d31db72fd5751f15b08ed19b1860010f38aaa77b7128ab0be578f117ff3b78d48d5995b0b
   --5c716544b0246dca90374012c8cced39cca254e061f5a8c3b2ede83dff6e
   Content-Disposition: form-data; name="package.gitlab-workhorse-upload"
   
   eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1cGxvYWQiOnsibWQ1IjoiMzkyOTNlYmQ5MjNiMTcxZWM4NDg0Zjg5MWQyNDAyOWIiLCJuYW1lIjoicGFja2FnZS5udXBrZyIsInBhdGgiOiIiLCJyZW1vdGVfaWQiOiIxNjM5MDY2ODk1LTM0NzktMDAwNC0xMDIzLTRiM2JlOTc0MjFlZTk2NGM5NjkwMWY0YzA4ZDkxMDAzIiwicmVtb3RlX3VybCI6Imh0dHA6Ly8xMjcuMC4wLjE6OTAwMC9wYWNrYWdlcy90bXAvdXBsb2Fkcy8xNjM5MDY2ODk1LTM0NzktMDAwNC0xMDIzLTRiM2JlOTc0MjFlZTk2NGM5NjkwMWY0YzA4ZDkxMDAzP1gtQW16LUV4cGlyZXM9MTUzMDBcdTAwMjZYLUFtei1EYXRlPTIwMjExMjA5VDE2MjEzNVpcdTAwMjZYLUFtei1BbGdvcml0aG09QVdTNC1ITUFDLVNIQTI1Nlx1MDAyNlgtQW16LUNyZWRlbnRpYWw9bWluaW8lMkYyMDIxMTIwOSUyRmdkayUyRnMzJTJGYXdzNF9yZXF1ZXN0XHUwMDI2WC1BbXotU2lnbmVkSGVhZGVycz1ob3N0XHUwMDI2WC1BbXotU2lnbmF0dXJlPTcwY2MwNWIzOGZlNzRlMzMwZTllM2U0NWE4MGE1OGRlYjdlOTc3MDRlZjMxNGJmY2EwOTg4MmU1NWQyODYyZTIiLCJzaGExIjoiNWExNTk3MTQ2MTZjNmU2Mzc4YWI3OTNiMzRjYTViN2NiZTY0ZTA4YyIsInNoYTI1NiI6IjcwMDI5ZDM4MzhjZGVkN2JmMjk5MDIzYzQyMmJjZGIwMGI2M2EyM2YxYTRlN2M1MzY5Y2NkYTJkNDhlN2FlNmQiLCJzaGE1MTIiOiIyZDQ0NjZmNGM4MmU3YjM2MzA5YjQzYTQwNGJiNjdjMDEzY2Y0OGY4MjNmNDY2NWVjZGI0NTQ2ZDMxZGI3MmZkNTc1MWYxNWIwOGVkMTliMTg2MDAxMGYzOGFhYTc3YjcxMjhhYjBiZTU3OGYxMTdmZjNiNzhkNDhkNTk5NWIwYiIsInNpemUiOiI0MDY1IiwidXBsb2FkX2R1cmF0aW9uIjoiMC4wMTE1NzQyNTcifSwiaXNzIjoiZ2l0bGFiLXdvcmtob3JzZSJ9.V2NoFiz6FVYlUFHvu6LL5M0TWSdshkOcsNZjS_STwgE
   --5c716544b0246dca90374012c8cced39cca254e061f5a8c3b2ede83dff6e
   Content-Disposition: form-data; name="package.name"
   
   package.nupkg
   --5c716544b0246dca90374012c8cced39cca254e061f5a8c3b2ede83dff6e
   Content-Disposition: form-data; name="package.remote_url"
   
   http://127.0.0.1:9000/packages/tmp/uploads/1639751506-21670-0002-9628-c1050a94854bc6fb445cd2b2e4ee543b?X-Amz-Expires=15300&X-Amz-Date=20211209T162135Z&X-Amz-Algorithm=AWS4-HMAC-SHA256&X-Amz-Credential=minio%2F20211209%2Fgdk%2Fs3%2Faws4_request&X-Amz-SignedHeaders=host&X-Amz-Signature=70cc05b38fe74e330e9e3e45a80a58deb7e97704ef314bfca09882e55d2862e2
   --5c716544b0246dca90374012c8cced39cca254e061f5a8c3b2ede83dff6e
   Content-Disposition: form-data; name="package.size"
   
   4065
   --5c716544b0246dca90374012c8cced39cca254e061f5a8c3b2ede83dff6e
   Content-Disposition: form-data; name="package.sha1"
   
   5a159714616c6e6378ab793b34ca5b7cbe64e08c
   --5c716544b0246dca90374012c8cced39cca254e061f5a8c3b2ede83dff6e
   Content-Disposition: form-data; name="package.sha256"
   
   70029d3838cded7bf299023c422bcdb00b63a23f1a4e7c5369ccda2d48e7ae6d
   --5c716544b0246dca90374012c8cced39cca254e061f5a8c3b2ede83dff6e
   Content-Disposition: form-data; name="package.path"
   
   
   --5c716544b0246dca90374012c8cced39cca254e061f5a8c3b2ede83dff6e--
   
   0
   
   
   ```
1. (Rails)
   ```
   POST   http://127.0.0.1:9000/packages/tmp/uploads/1639751506-21670-0002-9628-c1050a94854bc6fb445cd2b2e4ee543b (Content-Length 0)
   HEAD   http://127.0.0.1:9000/packages/tmp/uploads/1639751506-21670-0002-9628-c1050a94854bc6fb445cd2b2e4ee543b
   PUT    http://127.0.0.1:9000/packages/a0/51/a05198938c6ca8cd56289c6dba6bb8aaa68dfe8e0d7a37df2fb76e48eeba4244/packages/796/files/1294/package.nupkg (with `x-amz-copy-source` set)
   HEAD   http://127.0.0.1:9000/packages/a0/51/a05198938c6ca8cd56289c6dba6bb8aaa68dfe8e0d7a37df2fb76e48eeba4244/packages/796/files/1294/package.nupkg
   DELETE http://127.0.0.1:9000/packages/tmp/uploads/1639751506-21670-0002-9628-c1050a94854bc6fb445cd2b2e4ee543b
   ```

* After the `finalize` request
   * Why rails needs to create the entry on object storage (the POST with `Content-Length 0`)
* `finalize` request uses `multipart/form-data` content type.

### GCS

The object storage is now provided by an GCS bucket.

#### Body Upload

1. (WH) Original request:
   ```
   PUT /api/v4/projects/509/packages/generic/my_package/0.0.1/file.txt HTTP/1.1
   Host: gdk.test:8000
   Accept: */*
   Content-Length: 8
   Expect: 100-continue
   Private-Token: <PAT>
   User-Agent: curl/7.64.1
   
   bananas
   
   ```
1. (WH) Authorize request:
   ```
   PUT /api/v4/projects/509/packages/generic/my_package/0.0.1/file.txt/authorize HTTP/0.0
   Host: gdk.test:8000
   Accept: */*
   Expect: 100-continue
   Gitlab-Workhorse: 11-10-0cfa69752d8-74ffd66ae-ee-135315-g59a299328e3
   Gitlab-Workhorse-Api-Request: eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpc3MiOiJnaXRsYWItd29ya2hvcnNlIn0.y__tLHV0C92bicEJQZuGo0HGqbbJ0EbPY9Nv5bI0jKs
   Private-Token: <PAT>
   User-Agent: curl/7.64.1
   
   
   ```
1. (Rails) _No object storage interactions_
1. (WH) Authorize response:
   ```
   HTTP/1.0 200 OK
   Connection: close
   Cache-Control: max-age=0, private, must-revalidate
   Content-Type: application/vnd.gitlab-workhorse+json
   Etag: W/"ece9254de1855dd90b50575685cb58d2"
   Vary: Origin
   X-Content-Type-Options: nosniff
   X-Frame-Options: SAMEORIGIN
   X-Request-Id: 01FQ4G084M7ZR7EK493ZAB6QWF
   X-Runtime: 0.486393
   
   {"RemoteObject":{"Timeout":14400,"GetURL":"<GCS bucket URL>/tmp/uploads/1639754769-30334-0002-4465-26165d55691263e0575ca0fa30af4f73?GoogleAccessId=<GAID>\u0026Signature=05DqCWTFUXWihAIf1QhhryFwDTvOK3PnX3oN02EHv%2B8hZleDPS4BEJavt2NI%0AcxWGWhDlj6WCHyCXteT1QK%2FWvAm0GSR75COyzOiKdrgEAicfU%2F9dQ43r5W4H%0AsIjEzEWX7HdSuFD%2B8rIdmXFULfxDbvjrPTcRwESmox2ULThLiXUhcguKP2Dr%0A1M5LVYmk6Zxt9rP%2BQa%2BaYWa0eT5QpufxPS1XqeNY5aLqgUyKaAOlGPNXgrRJ%0AdEWk4UYvsqCScacdSFau3x8bHhuAUK4OfPU%2FrbygRHfYi3%2FcN3eFBsdBirJG%0AKsmVdEpyNH91ZFv18UVs3OGZsPfgles0L5jfePh1AQ%3D%3D\u0026Expires=1639770069","StoreURL":"<GCS bucket URL>/tmp/uploads/1639754769-30334-0002-4465-26165d55691263e0575ca0fa30af4f73?GoogleAccessId=<GAID>\u0026Signature=d9qr8IGosFLgiUrHvnsNG%2F8nZiwNQKEV25gqt2F3FMgzmEJv6MUt45hdZOCW%0ADyvGgCSCwJSam9%2BaeiinXELf80nwp%2BTjJo51eUu%2F%2BKqyJ2oiOy83Nh7zgUZA%0A6W4UOTkagYltsuryG9TZJ4TfvztkdiAntazIwjREAa1b%2FJ9MfDn8vgUlomhE%0A%2B9ONjuDE8dq5oe0vUYcH3w8W23AlLwBCQe5EGT7pg8wWt2OMqw7QM9%2BD7SLd%0AnBfFcY79tT4EMg3g7us1Yp67RUPqzWme%2FZgz6CLve3DGew4QbQ%2Fpk5ZPo%2FVx%0AR4bA1%2BffChKjUjaBTs%2FxwUqTJ8O5NmzL8La%2Fy1eRQw%3D%3D\u0026Expires=1639770069","DeleteURL":"<GCS bucket URL>/tmp/uploads/1639754769-30334-0002-4465-26165d55691263e0575ca0fa30af4f73?GoogleAccessId=<GAID>\u0026Signature=WdVM7Bke8yBg5b%2BbIxeRNClatEMH8FQbh1KtI64MFlNw25zOar%2F6ZYtUwyEC%0A2gnyeuK0LBJHMUqFpz%2Bl7%2Fvv7G5JcY3YK1j%2BW8si%2FkQsNAzlnpbau%2FG9lXOv%0AxabplU1xYQN7oZ9pgYcZngS%2B5r1yQwAfGGI3%2B9A%2BsRw3HY2FRURXG8JUzwT%2B%0ARIaEXR0uLe1FSIOhTfAj2lrBZ29j5Os%2BnpldmDu8Rl0rAh41Bav3igcoVaUc%0Ah7UgjlN6Xj9ezrp%2B3uRYhVMdf6K8uj7o%2By18VulzlQFxcVqQ29j0QanYEsQ1%0AyadtU%2ByxCzWmZTh0qy2PD4aCF5nbDhsIi%2FCDGbAzmA%3D%3D\u0026Expires=1639770069","CustomPutHeaders":true,"PutHeaders":{},"ID":"1639754769-30334-0002-4465-26165d55691263e0575ca0fa30af4f73"}}
   ```
1. (WH) Finalize request:
   ```
   PUT /api/v4/projects/509/packages/generic/my_package/0.0.1/file.txt HTTP/1.1
   Host: gdk.test:8000
   Accept: */*
   Content-Length: 8
   Content-Type: application/x-www-form-urlencoded
   Expect: 100-continue
   Gitlab-Workhorse-Multipart-Fields: eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJyZXdyaXR0ZW5fZmllbGRzIjp7ImZpbGUiOiIifSwiaXNzIjoiZ2l0bGFiLXdvcmtob3JzZSJ9.k4zvceZKFj4EehpnHsyMUfOZtBb1N60IkyXaeqV0CSA
   Private-Token: <PAT>
   User-Agent: curl/7.64.1
   
   file.gitlab-workhorse-upload=eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1cGxvYWQiOnsibWQ1IjoiM2UyNDJmYjcxNGQ4N2IzYzExMGM4NjQ3NmY1ZmY5NzIiLCJuYW1lIjoiIiwicGF0aCI6IiIsInJlbW90ZV9pZCI6IjE2Mzk3NTQ3NjktMzAzMzQtMDAwMi00NDY1LTI2MTY1ZDU1NjkxMjYzZTA1NzVjYTBmYTMwYWY0ZjczIiwicmVtb3RlX3VybCI6Imh0dHBzOi8vc3RvcmFnZS5nb29nbGVhcGlzLmNvbS9kZmVybmFuZGV6LXBhY2thZ2VzL3RtcC91cGxvYWRzLzE2Mzk3NTQ3NjktMzAzMzQtMDAwMi00NDY1LTI2MTY1ZDU1NjkxMjYzZTA1NzVjYTBmYTMwYWY0ZjczP0dvb2dsZUFjY2Vzc0lkPWFwaS04NzNAZGZlcm5hbmRlei01NDk0ZGQyYy5pYW0uZ3NlcnZpY2VhY2NvdW50LmNvbVx1MDAyNlNpZ25hdHVyZT0wNURxQ1dURlVYV2loQUlmMVFoaHJ5RndEVHZPSzNQblgzb04wMkVIdiUyQjhoWmxlRFBTNEJFSmF2dDJOSSUwQWN4V0dXaERsajZXQ0h5Q1h0ZVQxUUslMkZXdkFtMEdTUjc1Q095ek9pS2RyZ0VBaWNmVSUyRjlkUTQzcjVXNEglMEFzSWpFekVXWDdIZFN1RkQlMkI4cklkbVhGVUxmeERidmpyUFRjUndFU21veDJVTFRoTGlYVWhjZ3VLUDJEciUwQTFNNUxWWW1rNlp4dDlyUCUyQlFhJTJCYVlXYTBlVDVRcHVmeFBTMVhxZU5ZNWFMcWdVeUthQU9sR1BOWGdyUkolMEFkRVdrNFVZdnNxQ1NjYWNkU0ZhdTN4OGJIaHVBVUs0T2ZQVSUyRnJieWdSSGZZaTMlMkZjTjNlRkJzZEJpckpHJTBBS3NtVmRFcHlOSDkxWkZ2MThVVnMzT0dac1BmZ2xlczBMNWpmZVBoMUFRJTNEJTNEXHUwMDI2RXhwaXJlcz0xNjM5NzcwMDY5Iiwic2hhMSI6Ijk3MDMxODk2OGZlYjY0MGRhNzIzYjg4MjY4NjFlNDFmMDcxOGE0ODciLCJzaGEyNTYiOiJlNDkyOTU3MDJmN2RhODY3MDc3OGU5Yjk1YTI4MWI3MmI0MWIzMWNiMTZhZmEzNzYwMzRiNDVmNTlhMThlYTNmIiwic2hhNTEyIjoiODI5MDU2ZDRmZDMwMmNmMmZkYjQzZDlkY2JiMGE0MGNmMGNjZWRlNzJjZDNmYTQ3YjQyMGIxNWY0ODE4YzUzNDAwNDc3OWIyMTBkN2I2OTk4NjYxODliOTdmY2RlYjVjN2M5YzlmYTNlMmRlY2NjMzI3NGIzNGU4NWVhZDMxNjQiLCJzaXplIjoiOCIsInVwbG9hZF9kdXJhdGlvbiI6IjAuMjg3OTQ5MjE0In0sImlzcyI6ImdpdGxhYi13b3JraG9yc2UifQ.XTEyN6_0e1alAKpYV3LRA4so7_35eV976ETnFxiX6jE&file.md5=3e242fb714d87b3c110c86476f5ff972&file.name=&file.path=&file.remote_id=1639754769-30334-0002-4465-26165d55691263e0575ca0fa30af4f73&file.remote_url=<GCS Bucket URL>%2Ftmp%2Fuploads%2F1639754769-30334-0002-4465-26165d55691263e0575ca0fa30af4f73%3FGoogleAccessId%3Di-873%40<GAID>%26Signature%3D05DqCWTFUXWihAIf1QhhryFwDTvOK3PnX3oN02EHv%252B8hZleDPS4BEJavt2NI%250AcxWGWhDlj6WCHyCXteT1QK%252FWvAm0GSR75COyzOiKdrgEAicfU%252F9dQ43r5W4H%250AsIjEzEWX7HdSuFD%252B8rIdmXFULfxDbvjrPTcRwESmox2ULThLiXUhcguKP2Dr%250A1M5LVYmk6Zxt9rP%252BQa%252BaYWa0eT5QpufxPS1XqeNY5aLqgUyKaAOlGPNXgrRJ%250AdEWk4UYvsqCScacdSFau3x8bHhuAUK4OfPU%252FrbygRHfYi3%252FcN3eFBsdBirJG%250AKsmVdEpyNH91ZFv18UVs3OGZsPfgles0L5jfePh1AQ%253D%253D%26Expires%3D1639770069&file.sha1=970318968feb640da723b8826861e41f0718a487&file.sha256=e49295702f7da8670778e9b95a281b72b41b31cb16afa376034b45f59a18ea3f&file.sha512=829056d4fd302cf2fdb43d9dcbb0a40cf0ccede72cd3fa47b420b15f4818c534004779b210d7b699866189b97fcdeb5c7c9c9fa3e2deccc3274b34e85ead3164&file.size=8&file.upload_duration=0.287949214
   ```
1. (Rails)
   ```
   GET <GCS Bucket URL>/o/tmp%2Fuploads%2F1639754769-30334-0002-4465-26165d55691263e0575ca0fa30af4f73 (Metadata request)
   POST <GCS Bucket URL>/o/tmp%2Fuploads%2F1639754769-30334-0002-4465-26165d55691263e0575ca0fa30af4f73/copyTo/b/<GCS Bucket Name>/o/a0%2F51%2Fa05198938c6ca8cd56289c6dba6bb8aaa68dfe8e0d7a37df2fb76e48eeba4244%2Fpackages%2F791%2Ffiles%2F1302%2Ffile.txt
   DELETE <GCS Bucket URL>/o/tmp%2Fuploads%2F1639754769-30334-0002-4465-26165d55691263e0575ca0fa30af4f73
   ```

* Similar to what we see with S3
* We should use [rewrite](https://cloud.google.com/storage/docs/json_api/v1/objects/rewrite) instead of [copy](https://cloud.google.com/storage/docs/json_api/v1/objects/copy). See the copy page warning about large objects. `Generally, you should use the rewrite method instead of the copy method`

#### Multipart Upload

1. (WH) Original request:
   ```
   PUT /api/v4/projects/509/packages/nuget/ HTTP/1.1
   Host: gdk.test:8000
   Transfer-Encoding: chunked
   Accept-Encoding: gzip, deflate
   Authorization: Basic <encoded PAT>
   Content-Type: multipart/form-data; boundary="2eb404e3-3f12-48c5-b24e-8973b3b78811"
   User-Agent: NuGet xplat/5.8.0 (Darwin 20.6.0 Darwin Kernel Version 20.6.0: Mon Aug 30 06:12:21 PDT 2021; root:xnu-7195.141.6~3/RELEASE_X86_64)
   X-Nuget-Client-Version: 5.8.0
   X-Nuget-Session-Id: 4abeae08-f5ec-4955-8257-ae05f37fd514
   
   10c4
   --2eb404e3-3f12-48c5-b24e-8973b3b78811
   Content-Type: application/octet-stream
   Content-Disposition: form-data; name=package; filename=package.nupkg; filename*=utf-8''package.nupkg
   
   <gibberish file data>
   --2eb404e3-3f12-48c5-b24e-8973b3b78811--
   
   0
   
   
   ```
1. (WH) Authorize request:
   ```
   PUT /api/v4/projects/509/packages/nuget/authorize HTTP/0.0
   Host: gdk.test:8000
   Authorization: Basic <encoded PAT>
   Gitlab-Workhorse: 11-10-0cfa69752d8-74ffd66ae-ee-135315-g59a299328e3
   Gitlab-Workhorse-Api-Request: eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpc3MiOiJnaXRsYWItd29ya2hvcnNlIn0.y__tLHV0C92bicEJQZuGo0HGqbbJ0EbPY9Nv5bI0jKs
   User-Agent: NuGet xplat/5.8.0 (Darwin 20.6.0 Darwin Kernel Version 20.6.0: Mon Aug 30 06:12:21 PDT 2021; root:xnu-7195.141.6~3/RELEASE_X86_64)
   X-Nuget-Client-Version: 5.8.0
   X-Nuget-Session-Id: 4abeae08-f5ec-4955-8257-ae05f37fd514
   
   
   ```
1. (Rails) _No object storage interactions_
1. (WH) Authorize response:
   ```
   HTTP/1.0 200 OK
   Connection: close
   Cache-Control: max-age=0, private, must-revalidate
   Content-Type: application/vnd.gitlab-workhorse+json
   Etag: W/"d45f326d8ac6b3e1c2fd3b51848ee163"
   Vary: Origin
   X-Content-Type-Options: nosniff
   X-Frame-Options: SAMEORIGIN
   X-Request-Id: 01FQ4HFPTT3FKAMSEW34NCAMHQ
   X-Runtime: 0.636913
   
   {"RemoteObject":{"Timeout":14400,"GetURL":"<GCS Bucket URL>/tmp/uploads/1639756324-30334-0003-7202-da1cd7c224cee488677e49b5d141a940?GoogleAccessId=<GAID>\u0026Signature=E2M2WdUTbmkG%2BUH7iP1JWlHUn3SoWLXKb2pUVRdZwGNuB9gwhmPP6SptL51L%0AflO4lnu72aR%2BoMQy%2Fkg6D%2Bqtyog33WfclPKvoxognJj58GEg8x3o9RguJ9TY%0AyZRZwtfIdtb7Pa5C1KcrQJbI2JgAkJjFAMYtDaoJpJoCRW3Uy9HNweOJ2uMG%0Ah9YVQ7r5q93mOTVBkjEUib4QxDjBU3BoFeIEvzcYj0cVpOiwW4WKTp7KgDxF%0Abj1R22AgsFCHmN0WFpmNdArhu8IDlT8W%2FGoIuef3bkXLpjinhURCQVK0pGL7%0AN%2FEcW%2FSb%2FrkVw7omBuVGAvuIdTOOTjWKAgY2ZKil%2Fg%3D%3D\u0026Expires=1639771624","StoreURL":"<GCS Bucket URL>/tmp/uploads/1639756324-30334-0003-7202-da1cd7c224cee488677e49b5d141a940?GoogleAccessId=<GAID>\u0026Signature=Q5l06H%2B%2FzmNNhPp9vJdBdW9pijggVI1%2FkByXLyX6YwcNrOuIxDxRfn9%2F5WjQ%0AVW4nXjY0FTb672PRbLvU1ottt3BYiOzwUks%2Bq%2FN2gp7yRqpY1sB23O7pSTW4%0Aaf3KBAi58pQaF06MulF46q0OH10OPw%2BSP5r%2FgrzPTD2o3XoM7%2FEaMZKZ2YPb%0ApO9Rpg7dYmxbnWAYSJIKEa0LamPbX52WvAfRCQYH%2FvALPHqoH44TACqPD2NH%0AFxfYB3SgJox3Ha36DVPT8mHpOqsi%2FnEW552EsO%2BCCOjnS7WcNx5ZUKhtQMe2%0AMLJsTyBl0rQ7%2FNge5rD3kP%2BL%2FLzMlW%2BHfKpmx9l19g%3D%3D\u0026Expires=1639771624","DeleteURL":"<GCS Bucket URL>/tmp/uploads/1639756324-30334-0003-7202-da1cd7c224cee488677e49b5d141a940?GoogleAccessId=<GAID>\u0026Signature=o%2B%2B30hY28cNm3SjrhzHB9%2FoDkwU2xMOqmwpiqT649DZlFSy88Tx%2BVWshFSen%0AnFwirSTAk3XqJTlemA3rzzyPh%2B3OGeCg8NPt1jKaArNTKj7nemoRdEv1%2FhmK%0AFCD3fEtY%2FhWNaTqO1eHsJ63UrWz2FgIl5vPNrS%2BMCvtvivPW7AZtZnxV6pmQ%0A5Cf0ljQKkOtK1BF%2BhrcOWnUe4am4%2FW0ygWiSFPPaa6kLWTdnL4uuYvRwD%2FCd%0AvWD5nqCASkH%2F581U7cEDe%2FyMmruY94prVjS3EXPWpAQTvuSrqnLCfk4SwGlH%0AKsA6GpS%2FYhNxG82wc%2Fmkj1QwlnFGyshl%2BXojzgV3Ng%3D%3D\u0026Expires=1639771624","CustomPutHeaders":true,"PutHeaders":{},"ID":"1639756324-30334-0003-7202-da1cd7c224cee488677e49b5d141a940"},"MaximumSize":524288000}
   ```
1. (WH) Finalize request:
   ```
   PUT /api/v4/projects/509/packages/nuget/ HTTP/1.1
   Host: gdk.test:8000
   Transfer-Encoding: chunked
   Accept-Encoding: gzip, deflate
   Authorization: Basic <encoded PAT>
   Content-Type: multipart/form-data; boundary=73892e28f9d150241793fc24fc4c5a81cae49546f4f9fffa8c731c973ccd
   Gitlab-Workhorse-Multipart-Fields: eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJyZXdyaXR0ZW5fZmllbGRzIjp7InBhY2thZ2UiOiIifSwiaXNzIjoiZ2l0bGFiLXdvcmtob3JzZSJ9.oATwLhyIRWL8Y32pMqhG1lodvbTejJMzGKutA-q3dm8
   User-Agent: NuGet xplat/5.8.0 (Darwin 20.6.0 Darwin Kernel Version 20.6.0: Mon Aug 30 06:12:21 PDT 2021; root:xnu-7195.141.6~3/RELEASE_X86_64)
   X-Nuget-Client-Version: 5.8.0
   X-Nuget-Session-Id: 4abeae08-f5ec-4955-8257-ae05f37fd514
   
   f83
   --73892e28f9d150241793fc24fc4c5a81cae49546f4f9fffa8c731c973ccd
   Content-Disposition: form-data; name="package.md5"
   
   3784a71a8ba8cbe81d0ed309d11520da
   --73892e28f9d150241793fc24fc4c5a81cae49546f4f9fffa8c731c973ccd
   Content-Disposition: form-data; name="package.sha256"
   
   edccd4c3be0d7cdf68221e54c7c8d294bfc7014918f30cecd3d4e182d28467c1
   --73892e28f9d150241793fc24fc4c5a81cae49546f4f9fffa8c731c973ccd
   Content-Disposition: form-data; name="package.remote_id"
   
   1639756324-30334-0003-7202-da1cd7c224cee488677e49b5d141a940
   --73892e28f9d150241793fc24fc4c5a81cae49546f4f9fffa8c731c973ccd
   Content-Disposition: form-data; name="package.upload_duration"
   
   0.176004249
   --73892e28f9d150241793fc24fc4c5a81cae49546f4f9fffa8c731c973ccd
   Content-Disposition: form-data; name="package.name"
   
   package.nupkg
   --73892e28f9d150241793fc24fc4c5a81cae49546f4f9fffa8c731c973ccd
   Content-Disposition: form-data; name="package.path"
   
   
   --73892e28f9d150241793fc24fc4c5a81cae49546f4f9fffa8c731c973ccd
   Content-Disposition: form-data; name="package.remote_url"
   
   <GCS BUCKET URL>/tmp/uploads/1639756324-30334-0003-7202-da1cd7c224cee488677e49b5d141a940?GoogleAccessId=<GAID>&Signature=E2M2WdUTbmkG%2BUH7iP1JWlHUn3SoWLXKb2pUVRdZwGNuB9gwhmPP6SptL51L%0AflO4lnu72aR%2BoMQy%2Fkg6D%2Bqtyog33WfclPKvoxognJj58GEg8x3o9RguJ9TY%0AyZRZwtfIdtb7Pa5C1KcrQJbI2JgAkJjFAMYtDaoJpJoCRW3Uy9HNweOJ2uMG%0Ah9YVQ7r5q93mOTVBkjEUib4QxDjBU3BoFeIEvzcYj0cVpOiwW4WKTp7KgDxF%0Abj1R22AgsFCHmN0WFpmNdArhu8IDlT8W%2FGoIuef3bkXLpjinhURCQVK0pGL7%0AN%2FEcW%2FSb%2FrkVw7omBuVGAvuIdTOOTjWKAgY2ZKil%2Fg%3D%3D&Expires=1639771624
   --73892e28f9d150241793fc24fc4c5a81cae49546f4f9fffa8c731c973ccd
   Content-Disposition: form-data; name="package.sha1"
   
   c2d6865ef915b1922a8d1207b2cae49017c5c950
   --73892e28f9d150241793fc24fc4c5a81cae49546f4f9fffa8c731c973ccd
   Content-Disposition: form-data; name="package.sha512"
   
   f50109b3b90004481a407a61518d8ffe2553d1ff49b36a4c726f10d74dfd4f3975edb2a794f2ef280a546db329f0b966e2d9d41a795e26adced61b5e24021cd9
   --73892e28f9d150241793fc24fc4c5a81cae49546f4f9fffa8c731c973ccd
   Content-Disposition: form-data; name="package.gitlab-workhorse-upload"
   
   eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1cGxvYWQiOnsibWQ1IjoiMzc4NGE3MWE4YmE4Y2JlODFkMGVkMzA5ZDExNTIwZGEiLCJuYW1lIjoicGFja2FnZS5udXBrZyIsInBhdGgiOiIiLCJyZW1vdGVfaWQiOiIxNjM5NzU2MzI0LTMwMzM0LTAwMDMtNzIwMi1kYTFjZDdjMjI0Y2VlNDg4Njc3ZTQ5YjVkMTQxYTk0MCIsInJlbW90ZV91cmwiOiJodHRwczovL3N0b3JhZ2UuZ29vZ2xlYXBpcy5jb20vZGZlcm5hbmRlei1wYWNrYWdlcy90bXAvdXBsb2Fkcy8xNjM5NzU2MzI0LTMwMzM0LTAwMDMtNzIwMi1kYTFjZDdjMjI0Y2VlNDg4Njc3ZTQ5YjVkMTQxYTk0MD9Hb29nbGVBY2Nlc3NJZD1hcGktODczQGRmZXJuYW5kZXotNTQ5NGRkMmMuaWFtLmdzZXJ2aWNlYWNjb3VudC5jb21cdTAwMjZTaWduYXR1cmU9RTJNMldkVVRibWtHJTJCVUg3aVAxSldsSFVuM1NvV0xYS2IycFVWUmRad0dOdUI5Z3dobVBQNlNwdEw1MUwlMEFmbE80bG51NzJhUiUyQm9NUXklMkZrZzZEJTJCcXR5b2czM1dmY2xQS3ZveG9nbkpqNThHRWc4eDNvOVJndUo5VFklMEF5WlJad3RmSWR0YjdQYTVDMUtjclFKYkkySmdBa0pqRkFNWXREYW9KcEpvQ1JXM1V5OUhOd2VPSjJ1TUclMEFoOVlWUTdyNXE5M21PVFZCa2pFVWliNFF4RGpCVTNCb0ZlSUV2emNZajBjVnBPaXdXNFdLVHA3S2dEeEYlMEFiajFSMjJBZ3NGQ0htTjBXRnBtTmRBcmh1OElEbFQ4VyUyRkdvSXVlZjNia1hMcGppbmhVUkNRVkswcEdMNyUwQU4lMkZFY1clMkZTYiUyRnJrVnc3b21CdVZHQXZ1SWRUT09UaldLQWdZMlpLaWwlMkZnJTNEJTNEXHUwMDI2RXhwaXJlcz0xNjM5NzcxNjI0Iiwic2hhMSI6ImMyZDY4NjVlZjkxNWIxOTIyYThkMTIwN2IyY2FlNDkwMTdjNWM5NTAiLCJzaGEyNTYiOiJlZGNjZDRjM2JlMGQ3Y2RmNjgyMjFlNTRjN2M4ZDI5NGJmYzcwMTQ5MThmMzBjZWNkM2Q0ZTE4MmQyODQ2N2MxIiwic2hhNTEyIjoiZjUwMTA5YjNiOTAwMDQ0ODFhNDA3YTYxNTE4ZDhmZmUyNTUzZDFmZjQ5YjM2YTRjNzI2ZjEwZDc0ZGZkNGYzOTc1ZWRiMmE3OTRmMmVmMjgwYTU0NmRiMzI5ZjBiOTY2ZTJkOWQ0MWE3OTVlMjZhZGNlZDYxYjVlMjQwMjFjZDkiLCJzaXplIjoiNDA2NCIsInVwbG9hZF9kdXJhdGlvbiI6IjAuMTc2MDA0MjQ5In0sImlzcyI6ImdpdGxhYi13b3JraG9yc2UifQ.pD8J-OO4diT1DMYEM2XqSrupmvwThJEXXBg_teqWQjw
   --73892e28f9d150241793fc24fc4c5a81cae49546f4f9fffa8c731c973ccd
   Content-Disposition: form-data; name="package.size"
   
   4064
   --73892e28f9d150241793fc24fc4c5a81cae49546f4f9fffa8c731c973ccd--
   
   0
   
   
   ```
1. (Rails)
   ```
   GET <GCS Bucket URL>/o/tmp%2Fuploads%2F1639756324-30334-0003-7202-da1cd7c224cee488677e49b5d141a940 (Metadata request)
   POST <GCS Bucket URL>/o/tmp%2Fuploads%2F1639756324-30334-0003-7202-da1cd7c224cee488677e49b5d141a940/copyTo/b/<GCS Bucket Name>/o/a0%2F51%2Fa05198938c6ca8cd56289c6dba6bb8aaa68dfe8e0d7a37df2fb76e48eeba4244%2Fpackages%2F801%2Ffiles%2F1303%2Ffile.txt
   DELETE <GCS Bucket URL>/o/tmp%2Fuploads%2F1639756324-30334-0003-7202-da1cd7c224cee488677e49b5d141a940
   ```

* Similar to what we see with S3

### Questions / Main observations

* With an object storage provider (S3/GCS), could we avoid the copy/delete requests at the end of the `finalize` request?
   * This means that the `authorize` response already gives the final location of the file in object storage.
   * This also means that key generation can't rely on 
      * ActiveModel ids (as they can be non existent during the `authorize` call)
      * filenames 
   * The above means that we would rely on a purely random key
      * This is ok but some "metadata" such as the filename to use might need to be saved in the database.
* There are two sets of file uploads parameters used in the `finalize` call. Remove the ones not used?
   * We can but use a feature flag: the `authorize` response would have a response flag instructing Workhorse to have two or one set of file uploads parameters in the `finalize` request.
* Could we always use `application/x-www-form-urlencoded` in the `finalize` payload? (smaller payload)
   * A: We can't. In multipart uploads, the multipart request is hijacked and forwarded to rails. In body uploads, the `finalize` request is created from scratch and so, `application/x-www-form-urlencoded` was the easiest way.
* If we don't remove the copy at the end of `finalize` request, can we use `rewrite` instead of `copy` on GCS? (better support for larger files)
* No matter the outcome, Workhorse will (after the final response sent to the client) initiate a `DELETE` on the temporary location.
   * This is a guard as the `finalize` endpoint can fail = Rails will not move the file from temporary to final location.
